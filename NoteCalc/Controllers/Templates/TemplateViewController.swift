//
//  TemplateViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 08.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kTemplateScreen = "Template"

class TemplateViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewTop: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var close: UIButton!
    
    // MARK: Actions
    
    @IBAction func onDismissTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onUseTap(_ sender: Any) {
        if let noteId = NotesFiles.shared.save(textView.text!) {
            UserDefaults.standard.set(headerLabel.text, forKey: "\(kNotesDefaultsNoteTitle)\(noteId)")
            UserDefaults.standard.set(noteId, forKey: kNotesDefaultsNoteId)

            dismiss(animated: true, completion: {
                if #available(iOS 13.0, *) {
                    (UIApplication.shared.visibleViewController as? TemplatesViewController)?.closeIt = true
                    
                    (UIApplication.shared.visibleViewController as? UIAdaptivePresentationControllerDelegate)?.presentationControllerDidDismiss?(self.presentationController!)
                } else {
                    (UIApplication.shared.visibleViewController as? TemplatesViewController)?.closeLegacy()
                }
            })
        }
    }
    
    // MARK: Variables
    
    var calculationsView: UIView?
    
    var data: Any?
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTextView()
        
        headerView.layer.borderColor = kAppAccentBorderColor
        
        if let data = data as? [String:String] {
            if let title = data["label"], let file = data["file"] {
                headerLabel.text = title
                
                let fileURL = URL(fileURLWithPath: "\(Bundle.main.resourcePath!)/\(file).notecalc")
                    
                do {
                    let note = try String(contentsOf: fileURL, encoding: .utf8)
                    
                    textView.text = note
                    
                    render()
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        // MARK: Legacy popup
        
        if #available(iOS 13.0, *) {
            headerViewTop.constant = kAppHeaderViewTop
        } else {
            view.layer.cornerRadius = kAppLegacyPopupRadius
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ColorScheme.shared.controller = self
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // MARK: Setup
    
    private func setupTextView() {
        automaticallyAdjustsScrollViewInsets = false
        
        let font = Renderer.shared.font()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
        paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing
        
        textView.typingAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.textColor() , NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        textView.textContainerInset.right = (view.frame.size.width - kCalculatorTextViewMargin) / 2
        
        textView.contentInset.top = 30
        textView.contentInset.left = 20
        textView.contentInset.right = (view.frame.size.width - kCalculatorTextViewMargin) / 2 - kCalculatorTextViewMargin
        textView.contentInset.bottom = kCalculatorTextViewMargin
    }
    
    // MARK: Render
    
    private func render() {
        let renderer = Renderer()
        
        let render = renderer.render(textView.text, frameSizeWidth: (view.frame.size.width - kCalculatorTextViewMargin) / 2 + kCalculatorTextViewMargin)
        
        let range = textView.selectedRange
        
        textView.isScrollEnabled = false
        textView.attributedText = render.attributedText
        textView.selectedRange = range
        textView.isScrollEnabled = true
        
        calculationsView?.removeFromSuperview()
        calculationsView = render.calculationsView

        textView.addSubview(calculationsView!)
    }
    
}

// MARK: ColorScheme Delegate

extension TemplateViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            view.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            
            backgroundView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            headerView.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
            
            headerLabel.textColor = ColorScheme.shared.scheme.textColor()
            
            close.setImage(UIImage(named: ColorScheme.shared.scheme.modalCloseImage()), for: .normal)
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            }, completion: nil)
        } else {
            set()
        }
    }
    
}
