//
//  TemplatesViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 04.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kTemplatesScreen = "Templates"

let kTemplatesStandartCellHeight: CGFloat = 44
let kTemplatesSeparatorCellHeight: CGFloat = 32

class TemplatesViewController: UIViewController, UIAdaptivePresentationControllerDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backgroundView: UIView!
            
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerViewTop: NSLayoutConstraint!
    
    @IBOutlet weak var close: UIButton!
    
    // MARK: Actions
    
    @IBAction func onDismissTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Variables
    
    public var closeIt = false
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.layer.borderColor = kAppAccentBorderColor
        
        // MARK: Legacy popup
        
        if #available(iOS 13.0, *) {
            headerViewTop.constant = kAppHeaderViewTop
        } else {
            view.layer.cornerRadius = kAppLegacyPopupRadius
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ColorScheme.shared.controller = self
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        if !closeIt { return }
        
        dismiss(animated: true, completion: {
            if #available(iOS 13.0, *) {
                (UIApplication.shared.visibleViewController as? UIAdaptivePresentationControllerDelegate)?.presentationControllerDidDismiss?(self.presentationController!)
            }
        })
    }
    
    func closeLegacy() {
        dismiss(animated: true, completion: {
            (UIApplication.shared.visibleViewController as? CalculatorViewController)?.updateScreen()
        })
    }
    
    // MARK: Custom events
    
    @objc private func onCellTap(sender: UITapGestureRecognizer) {
        if let row = (sender.view as? UITableViewCell)?.tag {
            let data = templatesData[row]
            
            if data["file"] != nil {
                Router.shared.popup(kTemplateScreen, sender: self, data: data)
            }
        }
    }
    
}

// MARK: UITableView delegate

extension TemplatesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return templatesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = templatesData[indexPath.row]
        
        if let type = data["type"] {
            if let cell = tableView.dequeueReusableCell(withIdentifier: type) {
                cell.selectionStyle = .none
                
                if let cell = cell as? KindaSettingsCellView {
                    cell.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
                    
                    cell.label.textColor = ColorScheme.shared.scheme.textColor()
                    cell.label.alpha = data["file"] == nil ? 0.5 : 1
                    
                    cell.topDelimeter?.backgroundColor = ColorScheme.shared.scheme.separatorColor()
                    cell.bottomDelimeter?.backgroundColor = ColorScheme.shared.scheme.separatorColor()
                    
                    if let emoji = data["emoji"] {
                        cell.iconEmoji.text = emoji
                    }
                    
                    cell.label.text = data["label"]
                    
                    switch type {
                        case "Text":
                            cell.backgroundColor = .clear
                            cell.label.textColor = ColorScheme.shared.scheme.textSecondaryColor()
                        break
                        default: break
                    }
                    
                    if cell.topDelimeter != nil {
                        cell.topDelimeter.isHidden = data["delimTop"] == nil
                        cell.topDelimeterLeft.constant = data["delimTop"] == "cut" ? 10 : 0
                        
                        cell.bottomDelimeter.isHidden = data["delimBottom"] == nil
                        cell.bottomDelimeterLeft.constant = data["delimBottom"] == "cut" ? 10 : 0
                    }
                    
                    cell.tag = indexPath.row
                    
                    cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCellTap)))
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch templatesData[indexPath.row]["type"] {
            case "Template": return kTemplatesStandartCellHeight
            case "Separator": return kTemplatesSeparatorCellHeight
            default: return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = kAppTapOpacity
        })
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = 1
        })
    }

}

// MARK: ColorScheme Delegate

extension TemplatesViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            backgroundView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            headerView.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
            
            headerLabel.textColor = ColorScheme.shared.scheme.textColor()
            
            close.setImage(UIImage(named: ColorScheme.shared.scheme.modalCloseImage()), for: .normal)
            
            tableView.reloadData()
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            })
        } else {
            set()
        }
    }
    
}

