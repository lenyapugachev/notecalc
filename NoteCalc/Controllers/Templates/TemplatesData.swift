//
//  TemplatesData.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 04.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

let templatesData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("In the templates you can see examples of using many functions", comment: ""), "type": "Text"],
    ["label": NSLocalizedString("Share a bar bill", comment: ""), "emoji": "🍻", "file": "ShareBill", "type": "Template", "delimTop": "full"],
    ["label": NSLocalizedString("Financial accounting by day", comment: ""), "emoji": "💰", "file": "FinancesByDay", "type": "Template", "delimTop": "cut"],
    ["label": NSLocalizedString("Financial accounting by day and categories", comment: ""), "emoji": "💰", "file": "FinancesByDayCats", "type": "Template", "delimTop": "cut"],
    ["label": NSLocalizedString("Financial accounting by month", comment: ""), "emoji": "💰", "file": "FinancesByMonth", "type": "Template", "delimTop": "cut", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Income on stocks", comment: ""), "emoji": "💎", "file": "Stocks", "type": "Template", "delimTop": "full", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Shopping list", comment: ""), "emoji": "🛍", "file": "Shopping", "type": "Template", "delimTop": "full"],
    ["label": NSLocalizedString("Repair", comment: ""), "emoji": "🏠", "file": "Repair", "type": "Template", "delimTop": "cut", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Best time", comment: ""), "emoji": "👟️️", "file": "BestTime", "type": "Template", "delimTop": "full"],
    ["label": NSLocalizedString("Workouts", comment: ""), "emoji": "💪🏻️", "file": "Training", "type": "Template", "delimTop": "cut", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Travel planning", comment: ""), "emoji": "🏖️", "file": "Travel", "type": "Template", "delimTop": "full", "delimBottom": "full"],
    ["type": "Separator"]
]
