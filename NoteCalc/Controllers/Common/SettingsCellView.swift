//
//  KindaSettingsCellView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 13.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kSettingsCellPickerSigns = NSLocalizedString("symbols", comment: "")
let kSettingsCellPickerSignsAmount = 10

class KindaSettingsCellView: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var topDelimeter: UIView!
    @IBOutlet weak var bottomDelimeter: UIView!

    @IBOutlet weak var topDelimeterLeft: NSLayoutConstraint!
    @IBOutlet weak var bottomDelimeterLeft: NSLayoutConstraint!
    
    @IBOutlet weak var iconWidth: NSLayoutConstraint!
    
    @IBOutlet weak var picker: UIPickerView!
    
    @IBOutlet weak var iconRight: UIImageView!
    @IBOutlet weak var arrowRight: UIImageView!
    
    @IBOutlet weak var iconEmoji: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var value: UILabel!
    
    @IBOutlet weak var switcher: UISwitch!
    
    @IBAction func switcherChanged(sender: UISwitch) {
        switcherClosure()
    }
    
    var switcherClosure = {}
    
}

// MARK: UIPickerView delegate

extension KindaSettingsCellView: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return kSettingsCellPickerSignsAmount
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: "\(row)", attributes: [NSAttributedString.Key.foregroundColor: label.textColor])
    }

    func pickerView(_: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        UserDefaults.standard.set(row, forKey: "settingsanswerPrecision")
        
        value.text = "\(row) \(kSettingsCellPickerSigns)"
    }
    
}
