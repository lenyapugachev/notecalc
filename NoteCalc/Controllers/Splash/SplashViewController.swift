//
//  SplashViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 11.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kSplashXibFilename = "SplashSlideView"
let kSplashSlideCount = 4

let kSplashDotScaleActive: CGFloat = 1.25
let kSplashDotColor = UIColor.gray
let kSplashDotColorActive = UIColor.white

let kSplashNextButtonTitle = NSLocalizedString("Next", comment: "")
let kSplashNextButtonTitleLastSlide = NSLocalizedString("Start using", comment: "")

class SplashViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var scrollView: UIScrollView! {
        didSet {
            scrollView.delegate = self
        }
    }
    
    @IBOutlet weak var pageDots: UIStackView!
    
    @IBOutlet weak var page1Dot: UIView!
    @IBOutlet weak var page2Dot: UIView!
    @IBOutlet weak var page3Dot: UIView!
    @IBOutlet weak var page4Dot: UIView!
    
    @IBOutlet weak var nextButtonTop: NSLayoutConstraint!
    @IBOutlet weak var nextButton: AdvancedButton!
    
    // MARK: Actions
    
    @IBAction func onNextTap(_ sender: Any) {
        let newPage = page + 1
        
        if newPage > (kSplashSlideCount - 1) {
            Router.shared.go(kSubscriptionScreen, sender: self)
            
            return
        }
        
        scrollView.setContentOffset(CGPoint(x: CGFloat(newPage) * view.frame.width, y: 0), animated: true)
        
        setPage(newPage)
    }
    
    // MARK: Variables
    
    var slides: [SplashSlideView] = []
    var page: Int = 0
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        [scrollView, pageDots, nextButton].forEach { $0.alpha = 0 }
        
        // MARK: Responsive UI
        
        if view.frame.width < kAppSmallPhoneSize {
            nextButtonTop.constant = 20
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        slides = createSplashView()
        setupSplashView(slides: slides)
        
        setPage(page)
        
        // MARK: Check to start
        
        let start = UserDefaults.standard.bool(forKey: "start")
        
        if start {
            Router.shared.go(kCalculatorScreen, sender: self)
            
            Purchases.shared.receiptValidation()
        } else {
            UIView.animate(withDuration: kAppAnimationStandart, animations: {
                [self.scrollView, self.pageDots, self.nextButton].forEach { $0.alpha = 1 }
            })
        }
    }
    
    // MARK: Splash view
    
    private func createSplashView() -> [SplashSlideView] {
        if let splashXib = Bundle.main.loadNibNamed(kSplashXibFilename, owner: self, options: nil) as? [SplashSlideView] {
           return splashXib
        }
       
        return []
    }
        
    private func setupSplashView(slides: [SplashSlideView]) {
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: scrollView.frame.height)
        scrollView.isPagingEnabled = true
        
        for i in 0 ..< slides.count {
            slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView.frame.height)
            
            scrollView.addSubview(slides[i])
            
            // MARK: Responsive UI
            
            if view.frame.width < kAppMediumPhoneSize {
                if !hasTopNotch {
                    slides[i].top.constant = 60
                    slides[i].middle.constant = -10
                    slides[i].bottom.constant = -10
                    slides[i].header.font = .systemFont(ofSize: 24, weight: .semibold)
                    slides[i].text.font = .systemFont(ofSize: 14)
                } else {
                    slides[i].middle.constant = -10
                    slides[i].text.font = .systemFont(ofSize: 15)
                }
            }
            
            if view.frame.width < kAppSmallPhoneSize {
                slides[i].bottom.constant = -30
                slides[i].middle.constant = -20
                slides[i].header.font = .systemFont(ofSize: 20, weight: .semibold)
                slides[i].text.font = .systemFont(ofSize: 10)
            }
        }
    }
    
    private func setPage(_ index: Int) {
        page = index
        
        let dots = [page1Dot, page2Dot, page3Dot, page4Dot]
        
        dots.forEach { $0?.backgroundColor = kSplashDotColor }
        
        nextButton.setTitle(page == (kSplashSlideCount - 1) ? kSplashNextButtonTitleLastSlide : kSplashNextButtonTitle, for: .normal)
        
        UIView.animate(withDuration: kAppAnimationShort, delay: 0, options: .curveEaseOut, animations: {
            dots.forEach {
                $0?.transform = .identity
            }
        
            dots[index]?.backgroundColor = kSplashDotColorActive
            dots[index]?.transform = dots[index]?.transform.scaledBy(x: kSplashDotScaleActive, y: kSplashDotScaleActive) ?? .identity
        })
    }

}

// MARK: UIScrollView Delegate

extension SplashViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        setPage(Int(round(scrollView.contentOffset.x/view.frame.width)))
    }
    
}
