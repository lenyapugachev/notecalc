//
//  SplashSlideView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 13.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class SplashSlideView: UIView {
    
    // MARK: Outlets
    
    @IBOutlet weak var top: NSLayoutConstraint!
    @IBOutlet weak var middle: NSLayoutConstraint!
    @IBOutlet weak var bottom: NSLayoutConstraint!
    
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var text: UILabel!
    
}
