//
//  CalculatorAutocompleteCellView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 18.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class CalculatorAutocompleteCellView: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var tapView: UIView!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    
}
