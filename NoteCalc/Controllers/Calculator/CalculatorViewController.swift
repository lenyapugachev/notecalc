//
//  CalculatorViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kCalculatorScreen = "Calculator"

let kCalculatorCellIdentifier = "Cell"

let kCalculatorAutocompleteFunction = NSLocalizedString("Function", comment: "")
let kCalculatorAutocompleteOperator = NSLocalizedString("Operator", comment: "")
let kCalculatorAutocompleteMarket = NSLocalizedString("Market", comment: "")
let kCalculatorAutocompleteCurrency = NSLocalizedString("Currency", comment: "")
let kCalculatorAutocompleteUnit = NSLocalizedString("Unit", comment: "")
let kCalculatorAutocompleteTimezone = NSLocalizedString("Timezone", comment: "")

let kCalculatorHeaderSetTitle = NSLocalizedString("Set", comment: "")
let kCalculatorHeaderNoteTitle = NSLocalizedString("Note title", comment: "")
let kCalculatorCommentTitle = NSLocalizedString("Comment", comment: "")
let kCalculatorCommentEditTitle = NSLocalizedString("Edit", comment: "")
let kCalculatorCommentRemoveTitle = NSLocalizedString("Remove", comment: "")
let kCalculatorHeaderCancel = NSLocalizedString("Cancel", comment: "")

let kCalculatorContextMenuCopyWithAnswer = NSLocalizedString("Copy with answer", comment: "")
let kCalculatorContextMenuAddComment = NSLocalizedString("Add a comment", comment: "")

let kCalculatorAutocompleteMaxRows = 5
let kCalculatorAutocompleteRowHeight: CGFloat = 44
let kCalculatorAutocompletePadding: CGFloat = 5

let kCalculatorTextViewMargin: CGFloat = 40

var globalNoteText = ""

class CalculatorViewController: UIViewController, UIAdaptivePresentationControllerDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet var topButtons: [UIButton]!
    @IBOutlet var bottomButtons: [UIButton]!
    
    @IBOutlet weak var bottom: NSLayoutConstraint!
    
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    @IBOutlet weak var textView: UITextView!
    
    @IBOutlet weak var topGradient: GradientView!
    
    @IBOutlet weak var linkButtonsView: UIView!
    @IBOutlet weak var linkDeclineButton: UIButton!
    @IBOutlet weak var linkShortButton: UIButton!
    @IBOutlet weak var linkFullButton: UIButton!
    
    @IBOutlet var behindLinkViews: [UIView]!
    
    @IBOutlet weak var autocompleteView: UIView!
    @IBOutlet weak var autocompleteViewTop: NSLayoutConstraint!
    @IBOutlet weak var autocompleteViewHeight: NSLayoutConstraint!
    @IBOutlet weak var autocompleteTableView: UITableView!
    @IBOutlet weak var autocompleteGradient: GradientView!
    
    @IBOutlet weak var titleButton: UIButton!
    @IBOutlet weak var loader: UIImageView!
    
    @IBOutlet weak var sum: UILabel!
    
    @IBOutlet weak var additionalKeyboardView: UIView!
    @IBOutlet weak var additionalKeyboardTop: NSLayoutConstraint!
    
    @IBOutlet weak var additionalKeyboardSeparatorView: UIView!
    
    @IBOutlet weak var additionalKeyboardTopCollectionView: UICollectionView!
    @IBOutlet weak var additionalKeyboardBottomCollectionView: UICollectionView!
    
    @IBOutlet weak var copiedLabel: UILabel!
    
    // MARK: Actions
    
    @IBAction func onNotesTap(_ sender: Any) {
        minuteTimer?.invalidate()
        minuteTimer = nil
        
        Router.shared.go(kNotesScreen, sender: self)
    }
    
    @IBAction func onNewTap(_ sender: Any) {
        createNew()
    }
    
    @IBAction func onTitleTap(_ sender: Any) {
        changeTitle()
    }
    
    @IBAction func onTemplatesTap(_ sender: Any) {
        Router.shared.popup(kTemplatesScreen, sender: self)
    }
    
    @IBAction func onHelpTap(_ sender: Any) {
        Router.shared.popup(kHelpScreen, sender: self)
    }
    
    @IBAction func onLinkDeclineTap(_ sender: Any) {
        replaceLink("simple")
    }
    
    @IBAction func onLinkShortTap(_ sender: Any) {
        replaceLink("short")
    }
    
    @IBAction func onLinkFullTap(_ sender: Any) {
        replaceLink("full")
    }
    
    @IBAction func onShareTap(_ sender: Any) {
        if let id = UserDefaults.standard.string(forKey: kNotesDefaultsNoteId) {
            if var file = NotesFiles.shared.url(id: id) {
                if let title = UserDefaults.standard.string(forKey: "\(kNotesDefaultsNoteTitle)\(id)") {
                    if title.count > 0 {
                        let linkUrl = file.deletingLastPathComponent().appendingPathComponent("\(title.replacingOccurrences(of: " ", with: "-")).notecalc")
                        
                        do {
                            try FileManager.default.removeItem(at: linkUrl)
                        } catch {
                            print(error.localizedDescription)
                        }
                        
                        do {
                            try FileManager.default.linkItem(at: file, to: linkUrl)
                            
                            file = linkUrl
                        } catch {
                            print(error.localizedDescription)
                        }
                    }
                }
                
                let answers = getAllAnswers()
                
                globalNoteText = ""
                
                for (i,line) in textView.text!.lines.enumerated() {
                    globalNoteText += line
                    
                    if let answer = answers[i] {
                        globalNoteText += " = \(answer)"
                    }
                    
                    globalNoteText += "\n"
                }
                
                let activityViewController = UIActivityViewController(activityItems: [file], applicationActivities: [CopyNoteActivity()])
                
                activityViewController.excludedActivityTypes = [.copyToPasteboard]

                present(activityViewController, animated: true)
            }
        }
    }
    
    // MARK: Variables
    
    var calculationsView: UIView?
    
    var reloadOnAppear = true
    
    var timer: Timer?
    var minuteTimer: Timer?
    
    var autocompletions: [String:String] = [:]
    
    var noteId = ""
    
    var autoCompleteBlock = false
    
    var savedSelectedRange: NSRange?
    
    let copyHighlighterView = UIView()
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardFrameWillChangeNotificationReceived(_:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        
        ColorScheme.shared.controller = self
        
        Renderer.shared.calculatorController = self
        
        updateColorScheme(animated: false)
        
        setupTextView()
        
        renderEveryMinute()
        
        bottomSeparatorView.alpha = 0.6

        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
            if !UserDefaults.standard.bool(forKey: "reviewed") {
                let reviewOpenCount = UserDefaults.standard.integer(forKey: "reviewOpenCount")
                
                if reviewOpenCount == 7 {
                    UserDefaults.standard.set(0, forKey: "reviewOpenCount")
                    
                    Router.shared.popup(kReviewScreen, sender: self)
                } else {
                    UserDefaults.standard.set(reviewOpenCount + 1, forKey: "reviewOpenCount")
                }
            }
        })
        
        // Copy
        
        copyHighlighterView.alpha = 0
        copyHighlighterView.layer.cornerRadius = 4
        
        view.addSubview(copyHighlighterView)
        
        sum.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(onSumLongTap)))
        
        // Context menu
        
        let copyWithAnswerItem = UIMenuItem(title: kCalculatorContextMenuCopyWithAnswer, action: #selector(copyWithAnswer))
        let addCommentItem = UIMenuItem(title: kCalculatorContextMenuAddComment, action: #selector(addComment))
        
        UIMenuController.shared.menuItems = [copyWithAnswerItem, addCommentItem]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if reloadOnAppear {
            loader.rotate()
        } else {
            keyboardChange()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if reloadOnAppear {
            updateScreen()
            
            view.layoutIfNeeded()
            
            reloadOnAppear = false
        }
    }
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        updateScreen()
    }
    
    public func updateScreen() {
        ColorScheme.shared.controller = self
        
        updateColorScheme(animated: false)
        
        loadNote()
    }
    
    // MARK: Links handle
    
    private func replaceLink(_ info: String) {
        if let word = (textView as? UITextViewFixed)?.currentWord() {
            if var range = word.1 {
                savedSelectedRange = NSRange(location: range.upperBound + 1, length: 0)
                
                if range.location > 0 {
                    if let before = textView.text.rangeFromNSRange(nsRange: NSRange(location: range.location - 1, length: 1)) {
                        if textView.text[before.lowerBound..<before.upperBound] != "\n" {
                            _ = textView(textView, shouldChangeTextIn: NSRange(location: range.location, length: 0), replacementText: "\n")

                            textView.text.insert(contentsOf: "\n", at: String.Index(utf16Offset: range.location, in: textView.text))

                            range.location += 1

                            savedSelectedRange?.location += 1
                        }
                    }
                }
                
                if range.location < textView.text.utf16.count {
                    func addBreakAfter() {
                        _ = textView(textView, shouldChangeTextIn: NSRange(location: range.location + range.length, length: 0), replacementText: "\n")
                        
                        textView.text.insert(contentsOf: "\n", at: String.Index(utf16Offset: range.location + range.length, in: textView.text))

                        savedSelectedRange?.location += 1
                    }
                    
                    if let after = textView.text.rangeFromNSRange(nsRange: NSRange(location: range.location + range.length, length: 1)) {
                        if textView.text[after.lowerBound..<after.upperBound] != "\n" {
                            addBreakAfter()
                        }
                    } else {
                        addBreakAfter()
                    }
                }

                Renderer.shared.markupData[range] = RenderMarkup(range: range, text: word.0, originalLocation: range.location, originalLength: range.length, type: "link", data: info)

                render()
            }
        }
    }
    
    private func hideLinkButtons() {
        UIView.animate(withDuration: 0.15, animations: {
            self.linkButtonsView.alpha = 0
            self.behindLinkViews.forEach { $0.alpha = 1 }
        })
    }
    
    // MARK: Custom events
    
    @objc private func onCellTap(sender: UITapGestureRecognizer) {
        if let word = (sender.view as? CalculatorAutocompleteCellView)?.mainLabel.text {
            _ = (textView as? UITextViewFixed)?.currentWord(replace: word + " ")
        }

        closeAutocomplete()
    }
    
    // MARK: Сontext menu

    @objc private func copyWithAnswer() {
        if let range = textView.selectedTextRange, let selectedText = textView.text(in: range) {
            if let calculation = RenderExpression(selectedText, rendererPreparedData: Renderer.shared.preparedData).solve()?.0 {
                UIPasteboard.general.string = "\(selectedText) = \(calculation)"
            } else {
                UIPasteboard.general.string = "\(selectedText)"
            }
        }
    }
    
    @objc private func addComment() {
        if let range = textView.selectedTextRange, let selectedText = textView.text(in: range), let nsRange = textView.nsRange {
            let alert = UIAlertController(title: selectedText, message: "", preferredStyle: .alert)

            alert.addTextField(configurationHandler: { textField in
                textField.text = ""
                textField.keyboardAppearance = .dark
                textField.autocapitalizationType = .sentences
            })
            
            alert.addAction(UIAlertAction(title: kCalculatorCommentTitle, style: .default, handler: { action in
                if let comment = alert.textFields?.first?.text {
                    Renderer.shared.markupData[nsRange] = RenderMarkup(range: nsRange, text: ".", originalLocation: nsRange.location, originalLength: nsRange.length, type: "comment", data: comment)

                    self.render()
                }
                
                self.view.endEditing(true)
                
                self.keyboardChange()
            }))
            
            alert.addAction(UIAlertAction(title: kCalculatorHeaderCancel, style: .cancel, handler: { action in
                self.view.endEditing(true)
                
                self.keyboardChange()
            }))

            self.present(alert, animated: true)
        }
    }
    
    public func viewComment(_ commentView: (UIView,String,String,NSRange)) {
        let alert = UIAlertController(title: commentView.2, message: commentView.1, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: kCalculatorCommentEditTitle, style: .default, handler: { action in
            self.editComment(commentView)
        }))
        
        alert.addAction(UIAlertAction(title: kCalculatorCommentRemoveTitle, style: .default, handler: { action in
            self.removeComment(commentView.3)
        }))

        self.present(alert, animated: true)
        
        self.view.endEditing(true)
        
        self.keyboardChange()
    }
    
    private func removeComment(_ range: NSRange) {
        Renderer.shared.markupData.removeValue(forKey: range)

        self.render()
    }
    
    private func editComment(_ commentView: (UIView,String,String,NSRange)) {
        let alert = UIAlertController(title: commentView.2, message: "", preferredStyle: .alert)

        alert.addTextField(configurationHandler: { textField in
            textField.text = commentView.1
            textField.keyboardAppearance = .dark
            textField.autocapitalizationType = .sentences
        })
        
        alert.addAction(UIAlertAction(title: kCalculatorCommentTitle, style: .default, handler: { action in
            if let comment = alert.textFields?.first?.text {
                Renderer.shared.markupData[commentView.3] = RenderMarkup(range: commentView.3, text: ".", originalLocation: commentView.3.location, originalLength: commentView.3.length, type: "comment", data: comment)

                self.render()
            }
            
            self.view.endEditing(true)
            
            self.keyboardChange()
        }))
        
        alert.addAction(UIAlertAction(title: kCalculatorHeaderCancel, style: .cancel, handler: { action in
            self.view.endEditing(true)
            
            self.keyboardChange()
        }))

        self.present(alert, animated: true)
    }
    
    // MARK: Copy answer
    
    private func copyLabel(_ label: UILabel?) {
        if let label = label {
            UIPasteboard.general.string = label.text
            
            let offset: CGFloat = 2
            
            let origin = view.convert(label.frame.origin, to: nil)
            
            copyHighlighterView.frame = CGRect(x: 0, y: origin.y - offset + (label == sum ? 0 : (textView.frame.origin.y - textView.contentOffset.y)), width: label.sizeThatFits(CGSize(width: .greatestFiniteMagnitude, height: label.frame.height)).width + offset * 4, height: label.frame.height + offset * 2)
            
            copyHighlighterView.frame.origin.x = origin.x + (label == sum ? 0 : (kCalculatorTextViewMargin / 2)) + (label.frame.width - (copyHighlighterView.frame.width - offset * 4)) - offset * 2
                
            UIView.animate(withDuration: 0.3, animations: {
                self.copyHighlighterView.alpha = 1
                
                self.copiedLabel.alpha = 1
            })
            
            UIView.animate(withDuration: 0.2, delay: 1.3, animations: {
                self.copyHighlighterView.alpha = 0
                
                self.copiedLabel.alpha = 0
            })
        }
    }
    
    // MARK: Create new note
    
    private func createNew() {
        Renderer.shared.markupData.removeAll()
        
        if let noteId = NotesFiles.shared.save("") {
            self.noteId = noteId

            UserDefaults.standard.set(noteId, forKey: kNotesDefaultsNoteId)
        }

        calculationsView?.removeFromSuperview()

        titleButton.setTitle(kNotesNonameLabel, for: .normal)
        titleButton.setTitleColor(ColorScheme.shared.scheme.textColor().withAlphaComponent(0.5), for: .normal)

        textView.text = ""
        textView.becomeFirstResponder()
        
        sum.text = ""
    }
    
    // MARK: Load Note
    
    private func loadNote() {
        UIView.animate(withDuration: 0.2, animations: {
            self.titleButton.alpha = 0
            self.textView.alpha = 0
            self.loader.alpha = 1
        })
        
        if let id = UserDefaults.standard.string(forKey: kNotesDefaultsNoteId) {
            noteId = id
            
            if let text = NotesFiles.shared.open(id: id) {
                Renderer.shared.markupData.removeAll()
                
                render(text)
            }
            
            let title = UserDefaults.standard.string(forKey: "\(kNotesDefaultsNoteTitle)\(id)")
            
            titleButton.setTitle(title == nil || title?.count == 0 ? kNotesNonameLabel : title!, for: .normal)
        } else {
            createNew()
        }
        
        UIView.animate(withDuration: 0.2, delay: 1, animations: {
            self.titleButton.alpha = 1
            self.textView.alpha = 1
            self.loader.alpha = 0
        })
    }
    
    // MARK: Render
    
    private func render(_ text: String? = nil) {
        let text = text != nil ? text! : textView.text!
        
        let render = Renderer.shared.render(text, frameSizeWidth: (view.frame.size.width - kCalculatorTextViewMargin) / 2 + kCalculatorTextViewMargin)
        
        _ = NotesFiles.shared.save(render.rawText, id: noteId)
        
        let range = savedSelectedRange != nil ? savedSelectedRange! : textView.selectedRange
        
        textView.isScrollEnabled = false
        textView.attributedText = render.attributedText
        textView.selectedRange = range
        textView.isScrollEnabled = true
        
        savedSelectedRange = nil
        
        calculationsView?.removeFromSuperview()
        calculationsView = render.calculationsView

        textView.addSubview(calculationsView!)
        textView.bringSubviewToFront(calculationsView!)
        
        solveSum()
    }
    
    private func renderEveryMinute() {
        let date = Date()
        let calendar = Calendar.current
        var components = calendar.dateComponents([.era, .year, .month, .day, .hour, .minute], from: date)

        guard let minute = components.minute else { return }
        components.second = 0
        components.minute = minute + 1

        guard let nextMinute = calendar.date(from: components) else { return }

        minuteTimer = Timer(fire: nextMinute, interval: 60, repeats: true) { [weak self] timer in
            self?.render()
        }
        
        if let minuteTimer = minuteTimer {
            RunLoop.main.add(minuteTimer, forMode: RunLoop.Mode.default)
        }
    }
    
    private func getAllAnswers() -> [Int:String] {
        var answers: [Int:String] = [:]
        
        if let calculationsView = calculationsView {
            for label in calculationsView.subviews.sorted(by: { $0.frame.origin.y < $1.frame.origin.y }) {
                if let label = label as? UILabel {
                    answers[label.tag] = label.text
                }
            }
        }
        
        return answers
    }
    
    public func solveSum() {
        var expression = ""
        
        let answers = getAllAnswers()
        
        for key in answers.keys.sorted() {
            let answer = answers[key] ?? ""
            
            expression += expression.count > 0 ? " + \(answer)" : answer
        }
        
        let sum = Renderer.shared.solveSum(expression, label: self.sum)
        
        if let id = UserDefaults.standard.string(forKey: kNotesDefaultsNoteId) {
            if sum != nil {
                UserDefaults.standard.set(sum!, forKey: kNotesDefaultsNoteSum + id)
            } else {
                UserDefaults.standard.removeObject(forKey: kNotesDefaultsNoteSum + id)
            }
        }
    }
    
    // MARK: Autocomplete
    
    private func autocomplete(word: String) {
        if autoCompleteBlock { return }
        
        if word.count == 0 { return closeAutocomplete() }
        if !word[word.startIndex].isLetter { return closeAutocomplete() }
        
        autoCompleteBlock = true
        
        let pattern = "\\b" + NSRegularExpression.escapedPattern(for: word)
        
        let units = renderDataUnitsFirstForm.keys.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        let stocks = renderDataStocks.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        let currencies = renderDataCurrenciesFirstForm.keys.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        let functions = renderDataFunctions.keys.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        let operators = renderDataOperators.keys.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        let timezones = renderDataTimezones.map { $0.split(separator: "/").last!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "+", with: #"\+"#) }.filter { $0.count > 1 && $0.range(of: pattern, options: [.regularExpression, .caseInsensitive]) != nil }
        
        autocompletions.removeAll()
        
        for function in functions {
            autocompletions[function] = kCalculatorAutocompleteFunction
        }
        
        for operation in operators {
            autocompletions[operation] = kCalculatorAutocompleteOperator
        }
        
        for stock in stocks {
            autocompletions[stock] = kCalculatorAutocompleteMarket
        }
        
        for currency in currencies {
            autocompletions[currency] = kCalculatorAutocompleteCurrency
        }
        
        for unit in units {
            autocompletions[unit] = kCalculatorAutocompleteUnit
        }
        
        for timezone in timezones {
            autocompletions[timezone.replacingOccurrences(of: #"\+"#, with: "+")] = kCalculatorAutocompleteTimezone
        }
        
        if autocompletions.keys.count > 0 {
            DispatchQueue.main.async {
                let autocompletions = self.autocompletions
                
                if let selectedRange = self.textView.selectedTextRange {
                    let caretRect = self.textView.caretRect(for: selectedRange.end)
                    let windowRect = self.textView.convert(caretRect, to: nil)
                    
                    let cursor = windowRect.origin.y
                    let height = autocompletions.keys.count > kCalculatorAutocompleteMaxRows ? CGFloat(kCalculatorAutocompleteMaxRows) * kCalculatorAutocompleteRowHeight : CGFloat(autocompletions.keys.count) * kCalculatorAutocompleteRowHeight
                    
                    let bottomPosition = cursor + windowRect.height + kCalculatorAutocompletePadding
                    let topPosition = cursor - kCalculatorAutocompletePadding - height
                    
                    self.autocompleteViewTop.constant = (bottomPosition + height + kCalculatorAutocompletePadding) <= (self.textView.frame.origin.y + self.textView.frame.height) ? bottomPosition : topPosition
                    
                    self.view.layoutIfNeeded()
                    
                    self.autocompleteTableView.reloadData()
                    
                    self.autoCompleteBlock = false
                    
                    UIView.animate(withDuration: kAppAnimationStandart, delay: 0, options: .curveEaseInOut, animations: {
                        self.autocompleteView.alpha = 1
                    }, completion: nil)
                }
            }
        } else {
            autoCompleteBlock = false
            
            closeAutocomplete()
        }
    }
    
    private func closeAutocomplete() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: kAppAnimationShort, delay: 0, options: .curveEaseInOut, animations: {
                self.autocompleteView.alpha = 0
            }, completion: nil)
        }
    }
    
    // MARK: Change Title
    
    private func changeTitle() {
        let noteTitle = UserDefaults.standard.string(forKey: "\(kNotesDefaultsNoteTitle)\(noteId)")
        
        let alert = UIAlertController(title: kCalculatorHeaderNoteTitle, message: "", preferredStyle: .alert)

        alert.addTextField(configurationHandler: { textField in
            textField.text = noteTitle
            textField.keyboardAppearance = .dark
            textField.autocapitalizationType = .sentences
        })
        
        alert.addAction(UIAlertAction(title: kCalculatorHeaderSetTitle, style: .default, handler: { action in
            if let title = alert.textFields?.first?.text {
                UserDefaults.standard.set(title, forKey: "\(kNotesDefaultsNoteTitle)\(self.noteId)")
                
                self.titleButton.setTitle(title.count == 0 ? kNotesNonameLabel : title, for: .normal)
                self.titleButton.setTitleColor(title.count == 0 ? ColorScheme.shared.scheme.textColor().withAlphaComponent(0.5) : ColorScheme.shared.scheme.textColor(), for: .normal)
            }
            
            self.view.endEditing(true)
            
            self.keyboardChange()
        }))
        
        alert.addAction(UIAlertAction(title: kCalculatorHeaderCancel, style: .cancel, handler: { action in
            self.view.endEditing(true)
            
            self.keyboardChange()
        }))

        self.present(alert, animated: true)
    }
    
    // MARK: Keyboard handling
    
    @objc private func onKeyboardFrameWillChangeNotificationReceived(_ notification: Notification) {
        guard let userInfo = notification.userInfo,
              let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        else { return }
        
        keyboardChange(keyboardFrame: keyboardFrame)
    }
    
    private func keyboardChange(keyboardFrame: CGRect = CGRect.zero) {
        let keyboardEnabled = UserDefaults.standard.bool(forKey: "settingsadditionalKeyboard")

        bottom.constant = keyboardFrame.height < 100 ? 70 : (keyboardFrame.height + (keyboardEnabled ? 120 : 38))
        additionalKeyboardTop.constant = keyboardFrame.height < 100 ? 70 : (keyboardEnabled ? 34 : 0)
        
        additionalKeyboardView.isHidden = !keyboardEnabled
        
        if bottom.constant == 70 {
            hideLinkButtons()
        } else {
            textViewDidChangeSelection(textView)
        }

        UIView.animate(withDuration: kAppAnimationStandart, delay: 0, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    // MARK: Setup

    private func setupTextView() {
        automaticallyAdjustsScrollViewInsets = false
        
        let font = Renderer.shared.font()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
        paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing
        
//        textView.textContainer.replaceLayoutManager(LayoutManager())
        
        textView.typingAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.textColor(), NSAttributedString.Key.paragraphStyle: paragraphStyle]
        
        textView.textContainerInset.right = (view.frame.size.width - kCalculatorTextViewMargin) / 2
        
        textView.contentInset.top = 20
        textView.contentInset.left = 20
        textView.contentInset.right = (view.frame.size.width - kCalculatorTextViewMargin) / 2 - kCalculatorTextViewMargin
        textView.contentInset.bottom = kCalculatorTextViewMargin + 50
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(onTextViewTap))
        tap.delegate = self
        
        textView.addGestureRecognizer(tap)
        
        let longtap = UILongPressGestureRecognizer(target: self, action: #selector(onTextViewLongTap))
        
        textView.addGestureRecognizer(longtap)
    }

    @objc func onTextViewTap(gesture: UITapGestureRecognizer) {
        Renderer.shared.textViewTap(gesture.location(in: self.textView))
    }

    @objc func onTextViewLongTap(gesture: UIGestureRecognizer) {
        for answer in calculationsView!.subviews {
            let located = answer.frame.contains(gesture.location(in: self.textView))

            if located {
                copyLabel(answer as? UILabel)
            }
        }
    }

    @objc func onSumLongTap(gesture: UIGestureRecognizer) {
        copyLabel(sum)
    }
    
}

extension CalculatorViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

// MARK: UIScrollView delegate

extension CalculatorViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if textView == scrollView {
            copyHighlighterView.alpha = 0
            
            scrollView.contentOffset.x = -kCalculatorTextViewMargin / 2
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if textView.isScrollEnabled {
            closeAutocomplete()
        }
    }
    
}

// MARK: UITextViewView delegate

extension CalculatorViewController: UITextViewDelegate {
    
    func textViewDidChangeSelection(_ textView: UITextView) {
        if textView.isScrollEnabled { closeAutocomplete() }
        
        if textView.isFirstResponder {
            if let word = (textView as? UITextViewFixed)?.currentWord() {
                if let range = word.1 {
                    if word.0.matches("(https?:[A-Z0-9a-z\\.\\/_-]*)(\\?)?([a-zA-Z0-9@;:%_\\+.~#?&\\/=-−-]*)?") {
                        let markuped = Renderer.shared.markupData.keys.contains(range)
                        
                        if !markuped {
                            UIView.animate(withDuration: 0.15, animations: {
                                self.linkButtonsView.alpha = 1
                                self.behindLinkViews.forEach { $0.alpha = 0 }
                            })
                            
                            return
                        }
                        
                        Renderer.shared.highlightMarkupAtRange(range)
                    }
                }
            }
        }
        
        hideLinkButtons()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let word = (textView as? UITextViewFixed)?.currentWord() {
            DispatchQueue.global(qos: .background).async {
                self.autocomplete(word: word.0)
            }
        }
        
        timer?.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: kAppAnimationStandart, repeats: false, block: { _ in self.render() })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        Renderer.shared.fixMarkup(range: range, text: text)
        
        return true
    }
    
}

// MARK: UITableView delegate & datasource

extension CalculatorViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let number = autocompletions.keys.count
        
        autocompleteViewHeight.constant = number > kCalculatorAutocompleteMaxRows ? CGFloat(kCalculatorAutocompleteMaxRows) * kCalculatorAutocompleteRowHeight : CGFloat(number) * kCalculatorAutocompleteRowHeight
        autocompleteGradient.isHidden = number <= kCalculatorAutocompleteMaxRows
        
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kCalculatorCellIdentifier) as? CalculatorAutocompleteCellView {
            cell.selectionStyle = .none
    
            let key = Array(autocompletions.keys.sorted(by: { $0 < $1 }))[indexPath.row]
            
            if let word = (textView as? UITextViewFixed)?.currentWord() {
                if let autocompletion = autocompletions[key] {
                    cell.mainLabel.attributedText = Renderer.shared.autocompleteText(key, type: autocompletion, currentWord: word.0)
                }
            }
            
            cell.secondaryLabel.text = autocompletions[key]
            
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCellTap)))
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            (self.autocompleteTableView.cellForRow(at: indexPath) as? CalculatorAutocompleteCellView)?.tapView.alpha = 1
        })
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            (self.autocompleteTableView.cellForRow(at: indexPath) as? CalculatorAutocompleteCellView)?.tapView.alpha = 0
        })
    }
    
}

// MARK: ColorScheme Delegate

extension CalculatorViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            view.backgroundColor = ColorScheme.shared.scheme.backgroundSecondaryColor()
            topView.backgroundColor = ColorScheme.shared.scheme.backgroundSecondaryColor()
            
            copiedLabel.backgroundColor = ColorScheme.shared.scheme.backgroundSecondaryColor()
            copiedLabel.textColor = ColorScheme.shared.scheme.rendererCalculationColor()
            
            titleButton.setTitleColor(ColorScheme.shared.scheme.textColor().withAlphaComponent(0.5), for: .normal)
            
            textView.keyboardAppearance = ColorScheme.shared.scheme.keyboardAppearance()
            textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.textColor()]
            
            copyHighlighterView.backgroundColor = ColorScheme.shared.scheme.calculatorAnswerLabelCopyHighlighterColor()
            
            if let id = UserDefaults.standard.string(forKey: kNotesDefaultsNoteId) {
                if let title = UserDefaults.standard.string(forKey: "\(kNotesDefaultsNoteTitle)\(id)") {
                    if title.count > 0 {
                        titleButton.setTitleColor(ColorScheme.shared.scheme.textColor(), for: .normal)
                    }
                }
            }
            
            [linkFullButton, linkShortButton, linkDeclineButton].forEach { $0?.setTitleColor(ColorScheme.shared.scheme.textColor(), for: .normal) }
            linkButtonsView.backgroundColor = ColorScheme.shared.scheme.calculatorKeyboardColor()
            
            autocompleteView.backgroundColor = ColorScheme.shared.scheme.calculatorAutocompleteBackgroundColor()
            
            autocompleteView.layer.shadowColor = ColorScheme.shared.scheme.calculatorAutocompleteShadowColor().cgColor
            autocompleteView.layer.borderColor = ColorScheme.shared.scheme.calculatorAutocompleteBorderColor().cgColor
            
            autocompleteGradient.setGradientBackground(colorTop: ColorScheme.shared.scheme.backgroundSecondaryColor().withAlphaComponent(0), colorBottom: ColorScheme.shared.scheme.backgroundSecondaryColor())
            topGradient.setGradientBackground(colorTop: ColorScheme.shared.scheme.backgroundSecondaryColor(), colorBottom: ColorScheme.shared.scheme.backgroundSecondaryColor().withAlphaComponent(0))
            
            topButtons.forEach {
                $0.setTitleColor(ColorScheme.shared.scheme.calculatorTopBarTextColor(), for: .normal)
                $0.tintColor = ColorScheme.shared.scheme.calculatorTopBarTextColor()
            }
            
            bottomButtons.forEach {
                $0.tintColor = ColorScheme.shared.scheme.calculatorTopBarTextColor()
            }
            
            bottomSeparatorView.backgroundColor = ColorScheme.shared.scheme.calculatorBottomSeparatorColor()
            additionalKeyboardSeparatorView.backgroundColor = ColorScheme.shared.scheme.calculatorTopBarTextColor()
            
            sum.textColor = ColorScheme.shared.scheme.rendererCalculationColor()
            
            additionalKeyboardView.backgroundColor = ColorScheme.shared.scheme.calculatorKeyboardColor()
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            }, completion: nil)
        } else {
            set()
        }
    }
    
}

// MARK: ColorScheme Delegate

extension CalculatorViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 2 ? calculatorKeyboard2Data.count + 1 : calculatorKeyboard1Data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width / 10, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? CalculatorKeyboardButtonCellView {
            if collectionView.tag != 2 || indexPath.row > 0 {
                cell.char.setTitleColor(ColorScheme.shared.scheme.textColor(), for: .normal)
                
                let char = collectionView.tag == 2 ? calculatorKeyboard2Data[indexPath.row-1] : calculatorKeyboard1Data[indexPath.row]
                
                cell.char.setTitle(char, for: .normal)
                cell.char.setImage(nil, for: .normal)
                
                if char.count > 2 {
                    cell.char.titleLabel?.font = .systemFont(ofSize: 14, weight: .medium)
                }
                
                cell.tap = {
                    _ = self.textView(self.textView, shouldChangeTextIn: self.textView.selectedRange, replacementText: cell.char.title(for: .normal)!)
                    
                    self.textView.insertText(cell.char.title(for: .normal)!)
                }
            } else {
                cell.char.setTitle(nil, for: .normal)
                cell.char.setImage(UIImage(named: "calculatorHideKeyboard"), for: .normal)
                
                cell.char.tintColor = ColorScheme.shared.scheme.textColor()
                
                cell.tap = {
                    self.view.endEditing(true)
                    
                    self.keyboardChange()
                }
            }
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
}
