//
//  CopyNoteActivity.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 25.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

extension UIActivity.ActivityType {
    static let copyNote = UIActivity.ActivityType("com.lenyapugachev.NoteCalc.CopyNoteActivity")
}

class CopyNoteActivity: UIActivity {
    
    override class var activityCategory: UIActivity.Category {
        return .action
    }

    override var activityType: UIActivity.ActivityType? {
        return .copyNote
    }

    override var activityTitle: String? {
        return NSLocalizedString("Copy lines with answers", comment: "")
    }

    override var activityImage: UIImage? {
        return UIImage(named: "activityCopy")
    }
    
    override func canPerform(withActivityItems activityItems: [Any]) -> Bool {
        for case is URL in activityItems {
            return true
        }

        return false
    }
    
    override func perform() {
        UIPasteboard.general.string = globalNoteText
    }
    
}
