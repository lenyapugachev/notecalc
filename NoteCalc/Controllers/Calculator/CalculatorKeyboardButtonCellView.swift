//
//  CalculatorKeyboardButtonCellView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 03.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class CalculatorKeyboardButtonCellView: UICollectionViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var char: UIButton!
    
    // MARK: Actions
    
    @IBAction func charTap() {
        tap()
    }
    
    @IBAction func charIn() {
        UIView.animate(withDuration: 0.2, animations: {
            self.backgroundColor = ColorScheme.shared.scheme.calculatorAdditionalKeyboardActiveColor()
        })
    }
    
    @IBAction func charOut() {
        UIView.animate(withDuration: 0.2, animations: {
            self.backgroundColor = ColorScheme.shared.scheme.calculatorAdditionalKeyboardActiveColor().withAlphaComponent(0)
        })
    }
    
    // MARK: Variables
    
    var tap = {}
    
}
