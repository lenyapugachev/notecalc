//
//  CalculatorKeyboardData.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 04.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

let calculatorKeyboard1Data = [
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "0"
]

let calculatorKeyboard2Data = [
    "+",
    "-",
    "×",
    "÷",
    "%",
    "in",
    "(",
    ")",
    "$",
    "€",
    "£",
    "₽",
    "sin",
    "cos",
    "tan",
    "log"
]
