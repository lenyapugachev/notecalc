//
//  ReviewViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kReviewScreen = "Review"

class ReviewViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var mainLabel: UILabel!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewTop: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var reviewButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var close: UIButton!
    
    // MARK: Actions
    
    @IBAction func onDismissTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onReviewTap(_ sender: Any) {
        
        
        Purchases.shared.review()
    }
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.layer.borderColor = kAppAccentBorderColor
        
        // MARK: Legacy popup
        
        if #available(iOS 13.0, *) {
            headerViewTop.constant = kAppHeaderViewTop
        } else {
            view.layer.cornerRadius = kAppLegacyPopupRadius
        }
        
        if #available(iOS 10.3, *) {
            if UserDefaults.standard.bool(forKey: "reviewed") {
                reviewButton.isHidden = true
            }
        } else {
            reviewButton.isHidden = true
        }
        
        // MARK: Responsive UI
        
        if view.frame.width < kAppSmallPhoneSize {
            mainLabel.font = UIFont.systemFont(ofSize: 13)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ColorScheme.shared.controller = self
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
}

// MARK: ColorScheme Delegate

extension ReviewViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            view.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            
            backgroundView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            headerView.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
            
            headerLabel.textColor = ColorScheme.shared.scheme.textColor()
            mainLabel.textColor = ColorScheme.shared.scheme.textColor()
            
            close.setImage(UIImage(named: ColorScheme.shared.scheme.modalCloseImage()), for: .normal)
            
            backgroundImage.tintColor = ColorScheme.shared.scheme.textColor()
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            }, completion: nil)
        } else {
            set()
        }
    }
    
}
