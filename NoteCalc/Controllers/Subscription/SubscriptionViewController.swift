//
//  SubscriptionViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kSubscriptionScreen = "Subscription"

let kSubscriptionMonthly = NSLocalizedString("monthly", comment: "")
let kSubscriptionYearly = NSLocalizedString("yearly", comment: "")
let kSubscriptionMonth = NSLocalizedString("month", comment: "")
let kSubscriptionAfter = NSLocalizedString("after", comment: "")

class SubscriptionViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var topGradient: GradientView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var option1Border: UIView!
    @IBOutlet weak var option2Border: UIView!
    
    @IBOutlet weak var option1Center: UIView!
    @IBOutlet weak var option2Center: UIView!
    
    @IBOutlet weak var option1TopLabel: UILabel!
    @IBOutlet weak var option2TopLabel: UILabel!
    
    @IBOutlet weak var option1BottomLabel: UILabel!
    @IBOutlet weak var option2BottomLabel: UILabel!
    
    @IBOutlet weak var subscriptionButton: UIButton!
    
    @IBOutlet weak var nextPrice: UILabel!
    
    @IBOutlet var loader: UIView!
    @IBOutlet var indicator: UIView!
    
    // MARK: Actions
    
    @IBAction func onOption1Tap(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.option1Center.alpha = 1
            self.option2Center.alpha = 0
        })
        
        self.nextPrice.text = "\(self.option1BottomLabel.text!) \(kSubscriptionAfter)"
    }
    
    @IBAction func onOption2Tap(_ sender: Any) {
        UIView.animate(withDuration: 0.2, animations: {
            self.option1Center.alpha = 0
            self.option2Center.alpha = 1
        })
        
        self.nextPrice.text = "\(self.option2BottomLabel.text!) \(kSubscriptionAfter)"
    }
    
    @IBAction func onSubscriptionTap(_ sender: Any) {
        let option = self.option1Center.alpha == 1 ? "yearly" : "monthly"
        
        if Purchases.shared.isAuthorizedForPayments {
            Purchases.shared.makeSubscription(option)
        }
    }
    
    @IBAction func onNoSubscriptionTap(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "start")
        
        Router.shared.go(kCalculatorScreen, sender: self)
    }
    
    @IBAction func onRestoreSubscriptionTap(_ sender: Any) {
        Purchases.shared.restore()
    }
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topGradient.setGradientBackground(colorTop: view.backgroundColor!, colorBottom: view.backgroundColor!.withAlphaComponent(0))
        
        [option1Border, option2Border].forEach { $0?.layer.borderColor = UIColor.white.cgColor }
        
        option2Center.alpha = 0
        
        Purchases.shared.setTransactionCompletion { state in
            DispatchQueue.main.async {
                switch state {
                    case .purchased:
                        UserDefaults.standard.set(true, forKey: "start")
                        
                        Router.shared.go(kCalculatorScreen, sender: self)
                    break
                    case .failed:
                        self.showLoader(false)
                    break
                    case .purchasing:
                        self.showLoader()
                    break
                    default: break
                }
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Purchases.shared.fetchAvailableSubscriptions { subscriptions in
            DispatchQueue.main.async {
                self.showLoader(false)
                
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.maximumFractionDigits = 2
                formatter.groupingSeparator = " "
                
                if let yearly = subscriptions["yearly"] {
                    if let formattedValue = formatter.string(from: NSNumber(value: Double(yearly.0 / 12))) {
                        self.option1TopLabel.text = "\(formattedValue) \(yearly.1)/\(kSubscriptionMonth)"
                    }
                    
                    if let formattedValue = formatter.string(from: NSNumber(value: yearly.0)) {
                        self.option1BottomLabel.text = "\(formattedValue) \(yearly.1) \(kSubscriptionYearly)"
                        
                        self.nextPrice.text = "\(self.option1BottomLabel.text!) \(kSubscriptionAfter)"
                    }
                }
                
                if let monthly = subscriptions["monthly"] {
                    if let formattedValue = formatter.string(from: NSNumber(value: monthly.0)) {
                        self.option2TopLabel.text = "\(formattedValue) \(monthly.1)/\(kSubscriptionMonth)"
                    }
                    
                    if let formattedValue = formatter.string(from: NSNumber(value: monthly.0)) {
                        self.option2BottomLabel.text = "\(formattedValue) \(monthly.1) \(kSubscriptionMonthly)"
                    }
                }
            }
        }
    }
    
    private func showLoader(_ show: Bool = true) {
        UIView.animate(withDuration: 0.3, animations: {
            self.loader.alpha = show ? 0.1 : 0
            self.indicator.alpha = show ? 1 : 0
        })
        
        self.subscriptionButton.isEnabled = !show
    }
    
}
