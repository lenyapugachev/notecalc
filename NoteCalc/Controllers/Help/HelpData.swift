//
//  HelpData.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 16.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

var helpData: [(String,String,String,String)] = [
    (
    NSLocalizedString("Units Header", comment: ""),
    NSLocalizedString("Units Text", comment: ""),
    NSLocalizedString("Units Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Time zone Header", comment: ""),
    NSLocalizedString("Time zone Text", comment: ""),
    NSLocalizedString("Time zone Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Comments Header", comment: ""),
    NSLocalizedString("Comments Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Tags Header", comment: ""),
    NSLocalizedString("Tags Text", comment: ""),
    NSLocalizedString("Tags Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Operations Header", comment: ""),
    NSLocalizedString("Operations Text", comment: ""),
    "",
    NSLocalizedString("Operations Table", comment: "")
    ),
    (
    NSLocalizedString("Currency Header", comment: ""),
    NSLocalizedString("Currency Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Stocks Header", comment: ""),
    NSLocalizedString("Stocks Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Percentage Header", comment: ""),
    NSLocalizedString("Percentage Text", comment: ""),
    "",
    NSLocalizedString("Percentage Table", comment: "")
    ),
    (
    NSLocalizedString("Scales Header", comment: ""),
    NSLocalizedString("Scales Text", comment: ""),
    NSLocalizedString("Scales Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Variables Header", comment: ""),
    NSLocalizedString("Variables Text", comment: ""),
    NSLocalizedString("Variables Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Constants Header", comment: ""),
    "",
    "",
    NSLocalizedString("Constants Table", comment: "")
    ),
    (
    NSLocalizedString("Functions Header", comment: ""),
    NSLocalizedString("Functions Text", comment: ""),
    "",
    NSLocalizedString("Functions Table", comment: "")
    ),
    (
    NSLocalizedString("CSS Header", comment: ""),
    NSLocalizedString("CSS Text", comment: ""),
    NSLocalizedString("CSS Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Previous Result Header", comment: ""),
    NSLocalizedString("Previous Result Text", comment: ""),
    NSLocalizedString("Previous Result Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Sum Header", comment: ""),
    NSLocalizedString("Sum Text", comment: ""),
    NSLocalizedString("Sum Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Average Header", comment: ""),
    NSLocalizedString("Average Text", comment: ""),
    NSLocalizedString("Average Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Minimum and maximum Header", comment: ""),
    NSLocalizedString("Minimum and maximum Text", comment: ""),
    NSLocalizedString("Minimum and maximum Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("SI Header", comment: ""),
    NSLocalizedString("SI Text", comment: ""),
    NSLocalizedString("SI Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Date & Time Header", comment: ""),
    NSLocalizedString("Date & Time Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Temperature Header", comment: ""),
    NSLocalizedString("Temperature Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Length Header", comment: ""),
    NSLocalizedString("Length Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Volume Header", comment: ""),
    NSLocalizedString("Volume Text", comment: ""),
    NSLocalizedString("Volume Example", comment: ""),
    ""
    ),
    (
    NSLocalizedString("Weight Header", comment: ""),
    NSLocalizedString("Weight Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Angular Header", comment: ""),
    NSLocalizedString("Angular Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Data Header", comment: ""),
    NSLocalizedString("Data Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Images Header", comment: ""),
    NSLocalizedString("Images Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("Links Header", comment: ""),
    NSLocalizedString("Links Text", comment: ""),
    "",
    ""
    ),
    (
    NSLocalizedString("All function Header", comment: ""),
    NSLocalizedString("All function Text", comment: ""),
    "",
    ""
    )
]
