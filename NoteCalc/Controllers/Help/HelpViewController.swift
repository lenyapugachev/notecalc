//
//  HelpViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kHelpScreen = "Help"
let kHelpCellIdentifier = "Cell"

let kHelpDownLabel = "▼"
let kHelpLeftLabel = "◀︎"

let kHelpCellHeight: CGFloat = 44
let kHelpTitlePaddingLeft: CGFloat = 20
let kHelpTitlePaddingTop: CGFloat = 5
let kHelpTitleFontSize: CGFloat = 18

let kHelpHeaderColor = UIColor(red: 0.154, green: 0.154, blue: 0.154, alpha: 1)

class HelpViewController: UIViewController {
    
    // MARK: Outlets
        
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerViewTop: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var close: UIButton!
    
    // MARK: Actions
    
    @IBAction func onDismissTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Variables
    
    var openedTab: Int?
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.layer.borderColor = kAppAccentBorderColor
        
        // MARK: Legacy popup
        
        if #available(iOS 13.0, *) {
            headerViewTop.constant = kAppHeaderViewTop
        } else {
            view.layer.cornerRadius = kAppLegacyPopupRadius
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ColorScheme.shared.controller = self
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        Renderer.secondary = nil
    }
    
    // MARK: Custom events
    
    @objc func onHeaderTap(sender: UITapGestureRecognizer) {
        openedTab = sender.view?.tag == openedTab ? nil : sender.view?.tag
        
        tableView.reloadData()
    }
    
}

// MARK: UITableView delegate

extension HelpViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return helpData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return openedTab == section ? 1 : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kHelpCellIdentifier) {
            cell.selectionStyle = .none
            cell.isUserInteractionEnabled = true
            
            if let label = cell.subviews.first?.subviews.first as? UITextView {
                var string = helpData[indexPath.section].1
                
                var ranges: [NSRange] = []
                
                while let matches = string.matches(withPattern: "`([^`]*)`"), let match = matches.first {
                    if let oldRange = string.rangeFromNSRange(nsRange: match.range(at: 0)), let wordRange = string.rangeFromNSRange(nsRange: match.range(at: 1)) {
                        string = string.replacingOccurrences(of: "`\(string[wordRange])`", with: string[wordRange], options: [], range: oldRange)
                        
                        ranges.append(NSRange(location: match.range(at: 0).location, length: match.range(at: 1).length))
                    }
                }
                
                let attributedString = NSMutableAttributedString(string: string, attributes: [.foregroundColor: ColorScheme.shared.scheme.textColor(), .font: UIFont.systemFont(ofSize: 18)])

                for range in ranges {
                    let font = UIFont(name: "IBMPlexMono-Medium", size: 18) ?? .systemFont(ofSize: 18)
                    
                    attributedString.addAttribute(.font, value: font, range: range)
                    attributedString.addAttribute(.foregroundColor, value: ColorScheme.shared.scheme.rendererUnitsColor(), range: range)
                }
                
                if let matches = string.matches(withPattern: "(https?:[A-Z0-9a-z\\.\\/_-]*)(\\?)?([a-zA-Z0-9@;:%_\\+.~#?&\\/=-−-]*)?") {
                    for match in matches {
                        if let range = string.rangeFromNSRange(nsRange: match.range(at: 0)) {
                            attributedString.addAttribute(.link, value: string[range], range: match.range(at: 0))
                        }
                    }
                }

                label.attributedText = attributedString
                
                for subview in label.subviews {
                    if subview.tag == 111 {
                        subview.removeFromSuperview()
                    }
                }
                
                let renderer = Renderer()
                renderer.overrideFontDemo = true
                
                Renderer.secondary = renderer
                
                if helpData[indexPath.section].2.count != 0 {
                    let textHeight = label.sizeThatFits(CGSize(width: label.frame.width, height: CGFloat.greatestFiniteMagnitude)).height

                    let render = renderer.render("\n\n" + helpData[indexPath.section].2, frameSizeWidth: (view.frame.size.width - kCalculatorTextViewMargin) / 2 + kCalculatorTextViewMargin)
                    
                    let paragraphSeparatorStyle = NSMutableParagraphStyle()
                    paragraphSeparatorStyle.lineSpacing = kRendererParagraphStyleLineSpacing
                    paragraphSeparatorStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing - 16
                    
                    (render.attributedText as? NSMutableAttributedString)?.addAttribute(.paragraphStyle, value: paragraphSeparatorStyle, range: NSRange(location: 0, length: 2))
                    
                    let paragraphStyle = NSMutableParagraphStyle()
                    paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
                    paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing
                    paragraphStyle.tailIndent = (view.frame.size.width - kCalculatorTextViewMargin) / 2
                    
                    (render.attributedText as? NSMutableAttributedString)?.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 2, length: render.attributedText.length - 2))
                    
                    attributedString.append(render.attributedText)

                    label.attributedText = attributedString
                    
                    render.calculationsView.frame.origin.y += textHeight - 60
                    
                    render.calculationsView.tag = 111
                    
                    label.addSubview(render.calculationsView)
                }
                
                if helpData[indexPath.section].3.count != 0 {
                    if let data = helpData[indexPath.section].3.replacingOccurrences(of: "{BASE}", with: ColorScheme.shared.scheme.helpHtmlBaseColor()).replacingOccurrences(of: "{FUNCTION}", with: ColorScheme.shared.scheme.helpHtmlFunctionColor()).replacingOccurrences(of: "{BASIC}", with: ColorScheme.shared.scheme.helpHtmlBasicColor()).data(using: String.Encoding.utf8, allowLossyConversion: false) {
                        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
                            NSAttributedString.DocumentReadingOptionKey.characterEncoding : String.Encoding.utf8.rawValue,
                            NSAttributedString.DocumentReadingOptionKey.documentType : NSAttributedString.DocumentType.html
                        ]
                        
                        let htmlString = try? NSMutableAttributedString(data: data, options: options, documentAttributes: nil)
                        
                        if let htmlString = htmlString {
                            let paragraphString = NSMutableAttributedString(string: "\n\n", attributes: [:])
                            
                            let paragraphStyle = NSMutableParagraphStyle()
                            paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
                            paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing - 16
                            
                            paragraphString.addAttribute(.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: 2))
                            
                            attributedString.append(paragraphString)
                            
//                            htmlString.addAttribute(.foregroundColor, value: ColorScheme.shared.scheme.textColor(), range: NSRange(location: 0, length: htmlString.length))
                            htmlString.addAttribute(.font, value: renderer.font(), range: NSRange(location: 0, length: htmlString.length))
                            
                            attributedString.append(htmlString)
                        }

                        label.attributedText = attributedString
                    }
                }
            }
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: kHelpCellHeight))
        
        headerView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
        
        headerView.tag = section
        
        let titleLabel = UILabel(frame: headerView.frame)
        
        titleLabel.textColor = ColorScheme.shared.scheme.textColor()
        titleLabel.text = "\(section + 1). \(helpData[section].0)"
        titleLabel.font = .systemFont(ofSize: kHelpTitleFontSize, weight: .semibold)
        
        titleLabel.sizeToFit()
        
        titleLabel.frame.size.height = kHelpCellHeight
        
        titleLabel.frame.origin = CGPoint(x: kHelpTitlePaddingLeft, y: kHelpTitlePaddingTop)
        
        headerView.addSubview(titleLabel)
        
        let markerView = UIImageView(image: UIImage(named: "helpMarker"))
        
        markerView.frame.origin = CGPoint(x: titleLabel.bounds.width + titleLabel.frame.origin.x + 10, y: 24)
        
        markerView.tintColor = ColorScheme.shared.scheme.textColor()
        
        markerView.transform = CGAffineTransform(rotationAngle: openedTab == section ? CGFloat.pi : 0)
        
        headerView.addSubview(markerView)
        
        headerView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onHeaderTap)))
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return kHelpCellHeight
    }

}

// MARK: ColorScheme Delegate

extension HelpViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            view.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            
            backgroundView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            headerView.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
            
            headerLabel.textColor = ColorScheme.shared.scheme.textColor()
            
            close.setImage(UIImage(named: ColorScheme.shared.scheme.modalCloseImage()), for: .normal)
            
            tableView.reloadData()
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            }, completion: nil)
        } else {
            set()
        }
    }
    
}
