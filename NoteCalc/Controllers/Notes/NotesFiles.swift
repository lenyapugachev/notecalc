//
//  NotesFiles.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 20.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

// MARK: Constants

let kNotesFilesExtension = "notecalc"

class NotesFiles: NSObject {
    
    static public var shared = NotesFiles()
    
    public func search(_ text: String) -> [String] {
        var filteredList: [String] = []
        
        for file in list() {
            if let folder = folder() {
                do {
                    let content = try String(contentsOf: folder.appendingPathComponent(file), encoding: .utf8)
                    
                    if content.contains(text) {
                        filteredList.append(file)
                    }
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
        
        return filteredList
    }
    
    public func sync() -> Bool {
        let oldFolder = folder(another: true)
        let newFolder = folder()
        
        if oldFolder == nil || newFolder == nil { return false }
        
        do {
            let filePaths = try FileManager.default.contentsOfDirectory(atPath: newFolder!.path)
            for filePath in filePaths {
                try FileManager.default.removeItem(atPath: newFolder!.appendingPathComponent(filePath).path)
            }
            
            let oldFilePaths = try FileManager.default.contentsOfDirectory(atPath: oldFolder!.path)
            for filePath in oldFilePaths {
                try FileManager.default.moveItem(at: oldFolder!.appendingPathComponent(filePath), to: newFolder!.appendingPathComponent(filePath))
            }
        } catch {
            print(error.localizedDescription)
        }
        
        return true
    }
    
    public func list() -> [String] {
        if let folder = folder() {
            do {
                return try FileManager.default.contentsOfDirectory(atURL: folder, sortedBy: .modified, ascending: false) ?? []
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return []
    }
    
    public func remove(file: String) {
        if let folder = folder() {
            do {
                try FileManager.default.removeItem(at: folder.appendingPathComponent("\(file)"))
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    public func open(id: String) -> String? {
        if let url = url(id: id) {
            do {
                return try String(contentsOf: url, encoding: .utf8)
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    public func url(id: String) -> URL? {
        if let folder = folder() {
            return folder.appendingPathComponent("\(id).\(kNotesFilesExtension)")
        }
        
        return nil
    }
    
    public func save(_ text: String, id: String? = nil) -> String? {
        if let folder = folder() {
            do {
                var id = id
                
                if id == nil {
                    id = String((0..<24).map { _ in kAppGeneratorLetters.randomElement()! })
                }
                
                try text.write(to: folder.appendingPathComponent("\(id!).\(kNotesFilesExtension)"), atomically: false, encoding: .utf8)
                
                return id
            } catch {
                print(error.localizedDescription)
            }
        }
        
        return nil
    }
    
    private func folder(another: Bool = false) -> URL? {
        let icloud = UserDefaults.standard.bool(forKey: "settingsiCloud")
        
        if let url = (another ? !icloud : icloud) ? FileManager.default.url(forUbiquityContainerIdentifier: nil)?.appendingPathComponent("Documents") : FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            if !FileManager.default.fileExists(atPath: url.path) {
                do {
                    try FileManager.default.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
                } catch {
                    print(error.localizedDescription)
                }
            }
            
            return url
        }
        
        return nil
    }
    
}
