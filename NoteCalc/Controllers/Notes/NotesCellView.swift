//
//  NotesCellView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class NotesCellView: UITableViewCell {
    
    // MARK: Outlets
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var secondaryLabel: UILabel!
    
    // MARK: Variables
    
    var noteId: String!
    
}
