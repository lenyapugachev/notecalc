//
//  NotesViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kNotesScreen = "Notes"
let kNotesCellIdentifier = "Cell"

let kNotesDeleteButtonLabel = NSLocalizedString("Delete", comment: "")

let kNotesDeletePopupTitle = ""
let kNotesDeletePopupMessage = NSLocalizedString("Are you sure you want to delete a note?", comment: "")
let kNotesDeletePopupAction = NSLocalizedString("Delete", comment: "")
let kNotesDeletePopupCancel = NSLocalizedString("Cancel", comment: "")

let kNotesNonameLabel = NSLocalizedString("No name", comment: "")

let kNotesDefaultsNoteId = "NoteCalc_openedNoteId"
let kNotesDefaultsNoteTitle = "NoteCalc_title_"
let kNotesDefaultsNoteSum = "NoteCalc_sum_"

let kNotesCellHeight: CGFloat = 60
let kNotesSearchPaddingLeft: CGFloat = 44
let kNotesBottomBarHeight: CGFloat = 125

class NotesViewController: UIViewController, UIAdaptivePresentationControllerDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var topGradient: GradientView!
    @IBOutlet weak var bottomGradient: GradientView!
    
    @IBOutlet weak var reviewButton: AdvancedButton!
    
    @IBOutlet var bottomButtons: [UIButton]!
    
    @IBOutlet weak var bottomBarView: UIView!
    @IBOutlet weak var bottomBarHeight: NSLayoutConstraint!
    
    @IBOutlet weak var premiumButtonWidth: NSLayoutConstraint!
    @IBOutlet weak var premiumButtonPadding: NSLayoutConstraint!
    @IBOutlet weak var reviewButtonEqual: NSLayoutConstraint!
    
    // MARK: Actions
    
    @IBAction func onNewNoteTap(_ sender: Any) {
        if let noteId = NotesFiles.shared.save("") {
            UserDefaults.standard.set(noteId, forKey: kNotesDefaultsNoteId)
            
            Router.shared.go(kCalculatorScreen, sender: self)
        }
    }
    
    @IBAction func onPremiumTap(_ sender: Any) {
        Router.shared.go(kSubscriptionScreen, sender: self)
    }
    
    @IBAction func onReviewTap(_ sender: Any) {
        Router.shared.popup(kReviewScreen, sender: self)
    }
    
    @IBAction func onHelpTap(_ sender: Any) {
        Router.shared.popup(kHelpScreen, sender: self)
    }
    
    @IBAction func onSettingsTap(_ sender: Any) {
        Router.shared.popup(kSettingsScreen, sender: self)
    }
    
    // MARK: Variables
    
    var notesList = NotesFiles.shared.list() {
        didSet {
            tableView.reloadData()
        }
    }
    
    var scheme = DefaultColorScheme()
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: kNotesSearchPaddingLeft, height: 0))
        
        searchField.leftView = paddingView
        searchField.leftViewMode = .always
        
        bottomBarHeight.constant = kNotesBottomBarHeight
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onTap)))
        
        if Purchases.shared.isSubscription {
            reviewButtonEqual.isActive = false
            premiumButtonWidth.isActive = true
            premiumButtonPadding.constant = 0
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateScreen()
    }
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        updateScreen()
    }
    
    func updateLegacy() {
        updateScreen(animated: true)
    }
    
    private func updateScreen(animated: Bool = false) {
        ColorScheme.shared.controller = self
        
        updateColorScheme(animated: animated)
        
        notesList = NotesFiles.shared.list()
    }
    
    // MARK: Custom events
    
    @objc private func onTap() {
        view.endEditing(true)
    }
    
    @objc private func onCellTap(sender: UITapGestureRecognizer) {
        onTap()
        
        if let noteId = (sender.view as? NotesCellView)?.noteId {
            UserDefaults.standard.set(noteId, forKey: kNotesDefaultsNoteId)
            
            Router.shared.go(kCalculatorScreen, sender: self)
        }
    }
    
}

// MARK: UITextField delegate

extension NotesViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text, let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
           
            notesList = updatedText.count == 0 ? NotesFiles.shared.list() : NotesFiles.shared.search(updatedText)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        searchField.attributedPlaceholder = NSAttributedString(string: searchField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: UIColor.clear])
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        searchField.attributedPlaceholder = NSAttributedString(string: searchField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.searchFieldTextColor()])
    }
    
}

// MARK: UITableView delegate & datasource

extension NotesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: kNotesCellIdentifier) as? NotesCellView {
            cell.selectionStyle = .none
            
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCellTap)))
            
            let fileId = notesList[indexPath.row].replacingOccurrences(of: ".\(kNotesFilesExtension)", with: "")
            
            if let title = UserDefaults.standard.string(forKey: "\(kNotesDefaultsNoteTitle)\(fileId)") {
                cell.mainLabel.text = title.count > 0 ? title : kNotesNonameLabel
            } else {
                cell.mainLabel.text = kNotesNonameLabel
            }
            
            cell.mainLabel.textColor = ColorScheme.shared.scheme.textColor() 
            
            cell.secondaryLabel.text = UserDefaults.standard.string(forKey: kNotesDefaultsNoteSum + fileId) ?? ""
            
            cell.noteId = fileId
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = kAppTapOpacity
        })
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = 1
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return kNotesCellHeight
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteButton = UITableViewRowAction(style: .normal, title: kNotesDeleteButtonLabel, handler: { (action, indexPath) in
            let alert = UIAlertController(title: kNotesDeletePopupTitle, message: kNotesDeletePopupMessage, preferredStyle: .actionSheet)

            alert.addAction(UIAlertAction(title: kNotesDeletePopupAction, style: .destructive , handler: { action in
                NotesFiles.shared.remove(file: self.notesList[indexPath.row])
                
                self.notesList = NotesFiles.shared.list()
            }))

            alert.addAction(UIAlertAction(title: kNotesDeletePopupCancel, style: .cancel, handler: { action in }))

            self.present(alert, animated: true, completion: nil)

            return
        })

        deleteButton.backgroundColor = kAppWarningColor

        return [deleteButton]
    }
    
}

// MARK: ColorScheme Delegate

extension NotesViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            view.backgroundColor = ColorScheme.shared.scheme.backgroundSecondaryColor()
            bottomBarView.backgroundColor = ColorScheme.shared.scheme.backgroundSecondaryColor()
            
            topGradient.setGradientBackground(colorTop: ColorScheme.shared.scheme.backgroundSecondaryColor(), colorBottom: ColorScheme.shared.scheme.backgroundSecondaryColor().withAlphaComponent(0))
            bottomGradient.setGradientBackground(colorTop: ColorScheme.shared.scheme.backgroundSecondaryColor().withAlphaComponent(0), colorBottom: ColorScheme.shared.scheme.backgroundSecondaryColor())
            
            reviewButton.layer.borderColor = ColorScheme.shared.scheme.textColor().cgColor
            bottomButtons.forEach {
                $0.setTitleColor(ColorScheme.shared.scheme.textColor(), for: .normal)
                $0.tintColor = ColorScheme.shared.scheme.textColor()
            }
            
            searchView.backgroundColor = ColorScheme.shared.scheme.searchFieldBackgroundColor()
            searchField.attributedPlaceholder = NSAttributedString(string: searchField.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.searchFieldTextColor()])
            searchIcon.tintColor = ColorScheme.shared.scheme.searchFieldTextColor()
            
            searchField.keyboardAppearance = ColorScheme.shared.scheme.keyboardAppearance()
            
            tableView.reloadData()
        }
        
        if animated {
            topGradient.alpha = 0
            bottomGradient.alpha = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                set()
            }, completion: { _ in
                self.topGradient.alpha = 1
                self.bottomGradient.alpha = 1
            })
        } else {
            set()
        }
    }
    
}
