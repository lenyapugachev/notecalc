//
//  Purchases.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 22.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import StoreKit

let kPurchasesReceiptUrl = "https://sandbox.itunes.apple.com/verifyReceipt"
let kPurchasesKey = "2f2644f91b684188a3f577e7f7094643"

class Purchases: NSObject {
    
    static var shared = Purchases()
    
    private var productsRequest: SKProductsRequest?
    private var productsResponse: SKProductsResponse?
    
    private var productsCompletion: ([String:(Double,String)]) -> () = { _ in }
    private var transactionCompletion: (SKPaymentTransactionState) -> () = { _ in }
    
    var isAuthorizedForPayments: Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    var isSubscription: Bool {
        if let date = UserDefaults.standard.object(forKey: "subscription") as? Date {
            return date > Date()
        } else {
            return false
        }
    }
    
    override init() {
        super.init()
        
        SKPaymentQueue.default().add(self)
        
        receiptValidation()
    }
    
    public func fetchAvailableSubscriptions(_ completion: @escaping ([String:(Double,String)]) -> ()) {
        productsCompletion = completion
        
        productsRequest?.cancel()

        productsRequest = SKProductsRequest(productIdentifiers: ["monthly", "yearly"])

        productsRequest?.delegate = self
        productsRequest?.start()
    }
    
    public func setTransactionCompletion(_ completion: @escaping (SKPaymentTransactionState) -> ()) {
        transactionCompletion = completion
    }
    
    public func makeSubscription(_ identifier: String) {
        if let products = productsResponse?.products {
            for product in products {
                if product.productIdentifier == identifier {
                    let payment = SKPayment(product: product)
                    
                    SKPaymentQueue.default().add(payment)
                }
            }
        }
    }
    
    public func restore() {
        SKPaymentQueue.default().restoreCompletedTransactions()
        
        transactionCompletion(.purchasing)
    }
    
    public func receiptValidation() {
        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        if let receiptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0)) {
            let jsonDict: [String: AnyObject] = ["receipt-data": receiptString as AnyObject, "password": kPurchasesKey as AnyObject]
            
            do {
                let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
                let storeURL = URL(string: kPurchasesReceiptUrl)!
                
                var storeRequest = URLRequest(url: storeURL)
                storeRequest.httpMethod = "POST"
                storeRequest.httpBody = requestData
                
                let session = URLSession(configuration: URLSessionConfiguration.default)
                
                let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                    do {
                        let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                        
                        if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                            UserDefaults.standard.set(date, forKey: "subscription")
                            
                            self?.transactionCompletion(.purchased)
                        }
                    } catch let parseError {
                        print(parseError)
                    }
                })
                
                task.resume()
            } catch let parseError {
                print(parseError)
            }
        }
        
        transactionCompletion(.failed)
    }
    
    public func review() {
        if #available(iOS 10.3, *) {
            SKStoreReviewController.requestReview()
            
            UserDefaults.standard.set(true, forKey: "reviewed")
        }
    }
    
    private func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
        }
        
        return nil
    }
    
}

extension Purchases: SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        productsResponse = response
        
        var products: [String:(Double,String)] = [:]
        
        for product in response.products {
            products[product.productIdentifier] = (product.price.doubleValue, (product.priceLocale.currencySymbol ?? product.priceLocale.currencyCode) ?? "")
        }
        
        productsCompletion(products)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction:AnyObject in transactions {
            if let trans = transaction as? SKPaymentTransaction {
                switch trans.transactionState {
                    case .purchased:
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        
                        self.receiptValidation()
                    break
                    case .failed:
                        SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
                        
                        transactionCompletion(trans.transactionState)
                    break
                    case .restored:
                        SKPaymentQueue.default().restoreCompletedTransactions()
                    break
                    default: break
                }
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        transactionCompletion(.failed)
    }
    
}
