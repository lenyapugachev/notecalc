//
//  Router.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kRouterStoryboard = "Main"

class Router: NSObject, UIAdaptivePresentationControllerDelegate {
    
    static public var shared = Router()
    
    private let storyboard = UIStoryboard(name: kRouterStoryboard, bundle: nil)
    
    func go(_ screen: String, sender: UIViewController, animated: Bool = true) {
        let viewController = storyboard.instantiateViewController(withIdentifier: screen)
        
        viewController.modalPresentationStyle = .currentContext
        viewController.modalTransitionStyle = .crossDissolve
        
        sender.present(viewController, animated: animated, completion: nil)
    }
    
    func popup(_ screen: String, sender: UIViewController, animated: Bool = true, data: Any? = nil) {
        let viewController = storyboard.instantiateViewController(withIdentifier: screen)
        
        (viewController as? TemplateViewController)?.data = data
        
        if #available(iOS 13.0, *) {
            viewController.presentationController?.delegate = sender as? UIAdaptivePresentationControllerDelegate
        } else {
            viewController.modalPresentationStyle = .overCurrentContext
        }
        
        sender.present(viewController, animated: animated, completion: nil)
    }
    
    func navigateSettings(_ screen: String, sender: UIViewController, settingsData: [[String:Any]], settingsLabel: String, animated: Bool = true) {
        if let viewController = storyboard.instantiateViewController(withIdentifier: screen) as? SettingsViewController {
            viewController.settingsData = settingsData
            
            viewController.header = settingsLabel
            
            sender.navigationController?.pushViewController(viewController, animated: animated)
        }
    }
    
    func navigateBack(sender: UIViewController, animated: Bool = true) {
        sender.navigationController?.popViewController(animated: animated)
    }
    
}
