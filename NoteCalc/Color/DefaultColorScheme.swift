//
//  DefaultColorScheme.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 30.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class DefaultColorScheme: NSObject {
    
    public func statusBarStyle() -> UIStatusBarStyle {
        return .lightContent
    }
    
    public func keyboardAppearance() -> UIKeyboardAppearance {
        return .dark
    }
    
    public func modalCloseImage() -> String {
        return "modalClose"
    }
    
    public func helpHtmlBaseColor() -> String {
        return "939393"
    }
    
    public func helpHtmlFunctionColor() -> String {
        return "94E2FF"
    }
    
    public func helpHtmlBasicColor() -> String {
        return "FFFFFF"
    }
    
    public func backgroundMainColor() -> UIColor {
        return UIColor(red: 0.154, green: 0.154, blue: 0.154, alpha: 1)
    }
    
    public func backgroundSecondaryColor() -> UIColor {
        return UIColor(red: 0.149, green: 0.149, blue: 0.149, alpha: 1)
    }
    
    public func backgroundAdditionalColor() -> UIColor {
        return self.backgroundSecondaryColor()
    }
    
    public func backgroundPopupAdditionalColor() -> UIColor {
        return UIColor(red: 0.183, green: 0.183, blue: 0.183, alpha: 1)
    }
    
    public func foregroundColor() -> UIColor {
        return UIColor(red: 0.922, green: 0.922, blue: 0.961, alpha: 1)
    }
    
    public func textColor() -> UIColor {
        return UIColor.white
    }
    
    public func textSecondaryColor() -> UIColor {
        return UIColor(red: 0.922, green: 0.922, blue: 0.961, alpha: 0.6)
    }
    
    public func separatorColor() -> UIColor {
        return UIColor(red: 0.329, green: 0.329, blue: 0.345, alpha: 0.65)
    }
    
    public func searchFieldBackgroundColor() -> UIColor {
        return UIColor(red: 0.346, green: 0.347, blue: 0.35, alpha: 1)
    }
    
    public func searchFieldTextColor() -> UIColor {
        return UIColor(red: 1, green: 1, blue: 1, alpha: 0.5)
    }
    
    public func calculatorTopBarTextColor() -> UIColor {
        return UIColor(red: 0.258, green: 0.258, blue: 0.258, alpha: 1)
    }
    
    public func calculatorAutocompleteBackgroundColor() -> UIColor {
        return UIColor(red: 0.267, green: 0.267, blue: 0.267, alpha: 1)
    }
    
    public func calculatorAutocompleteShadowColor() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.25)
    }
    
    public func calculatorAutocompleteBorderColor() -> UIColor {
        return UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 0)
    }
    
    public func calculatorBottomSeparatorColor() -> UIColor {
        return UIColor(red: 0.179, green: 0.179, blue: 0.179, alpha: 1)
    }
    
    public func calculatorKeyboardColor() -> UIColor {
        return UIColor(red: 0.183, green: 0.183, blue: 0.183, alpha: 1)
    }
    
    public func calculatorAdditionalKeyboardActiveColor() -> UIColor {
        return UIColor(red: 0.267, green: 0.267, blue: 0.267, alpha: 1)
    }
    
    public func calculatorAnswerLabelCopyHighlighterColor() -> UIColor {
        return UIColor(red: 0.394, green: 0.729, blue: 0.295, alpha: 0.2)
    }
    
    public func rendererCalculationColor() -> UIColor {
        return UIColor(red: 0.394, green: 0.729, blue: 0.295, alpha: 1)
    }
    
    public func rendererCommentHighlightColor() -> UIColor {
        return UIColor(red: 233/255, green: 233/255, blue: 233/255, alpha: 0.25)
    }
    
    public func rendererCommentColor() -> UIColor {
        return UIColor(red: 134/255, green: 134/255, blue: 134/255, alpha: 1)
    }
    
    public func rendererTitleColor() -> UIColor {
        return UIColor(red: 1, green: 148/255, blue: 181/255, alpha: 1)
    }
    
    public func rendererUnitsColor() -> UIColor {
        return UIColor(red: 167/255, green: 224/255, blue: 1, alpha: 1)
    }
    
    public func rendererFunctionsColor() -> UIColor {
        return UIColor(red: 123/255, green: 168/255, blue: 1, alpha: 1)
    }
    
    public func rendererLinkBackgroundColor() -> UIColor {
        return UIColor(red: 0.846, green: 0.846, blue: 0.846, alpha: 1)
    }
    
    public func rendererLinkActiveBackgroundColor() -> UIColor {
        return UIColor(red: 0.746, green: 0.746, blue: 0.746, alpha: 1)
    }
    
    public func rendererLinkColor() -> UIColor {
        return UIColor(red: 0.079, green: 0.079, blue: 0.079, alpha: 1)
    }
    
    public func calculatorLinkButtonsTextColor() -> UIColor {
        return UIColor.white
    }
    
    public func calculatorLinkButtonsBackgroundColor() -> UIColor {
        return UIColor(red: 0.183, green: 0.183, blue: 0.183, alpha: 1)
    }
    
}
