//
//  ColorScheme.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 30.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

protocol ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool)
    
}

class ColorScheme: NSObject {
    
    static public var shared = ColorScheme()
    
    public var scheme = DefaultColorScheme()
    
    public var controller: ColorSchemeDelegate! {
        didSet {
            if !(controller is SettingsViewController) {
                UIApplication.shared.setStatusBarStyle(scheme.statusBarStyle(), animated: true)
            }
            
            if !(controller is NotesViewController) {
                controller.updateColorScheme(animated: false)
            }
        }
    }
    
    override init() {
        super.init()
        
        if let value = UserDefaults.standard.string(forKey: "settingscolorScheme") {
            checkColorScheme(value: value)
        }
    }
    
    public func setColorScheme(_ value: String, animated: Bool) {
        checkColorScheme(value: value)
        
        controller.updateColorScheme(animated: animated)
    }
    
    private func checkColorScheme(value: String) {
        scheme = value == "light" ? LightColorScheme() : DefaultColorScheme()
    }
    
}
