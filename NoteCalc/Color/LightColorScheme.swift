//
//  LightColorScheme.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 30.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class LightColorScheme: DefaultColorScheme {
    
    override func statusBarStyle() -> UIStatusBarStyle {
        return .default
    }
    
    override func keyboardAppearance() -> UIKeyboardAppearance {
        return .light
    }
    
    override func modalCloseImage() -> String {
        return "modalCloseLight"
    }
    
    override func helpHtmlBaseColor() -> String {
        return "828284"
    }
    
    override func helpHtmlFunctionColor() -> String {
        return "0093C2"
    }
    
    override func helpHtmlBasicColor() -> String {
        return "000000"
    }
    
    override func backgroundMainColor() -> UIColor {
        return UIColor(red: 0.937, green: 0.937, blue: 0.957, alpha: 1)
    }
    
    override func backgroundSecondaryColor() -> UIColor {
        return UIColor.white
    }
    
    override func backgroundAdditionalColor() -> UIColor {
        return UIColor(red: 0.961, green: 0.961, blue: 0.961, alpha: 1)
    }
    
    override func backgroundPopupAdditionalColor() -> UIColor {
        return UIColor(red: 0.99, green: 0.99, blue: 0.99, alpha: 1)
    }
    
    override func foregroundColor() -> UIColor {
        return UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 1)
    }
    
    override func textColor() -> UIColor {
        return UIColor.black
    }
    
    override func textSecondaryColor() -> UIColor {
        return UIColor(red: 0.235, green: 0.235, blue: 0.263, alpha: 0.6)
    }
    
    override func separatorColor() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    
    override func searchFieldBackgroundColor() -> UIColor {
        return UIColor(red: 0.942, green: 0.942, blue: 0.942, alpha: 1)
    }
    
    override func searchFieldTextColor() -> UIColor {
        return UIColor(red: 0.079, green: 0.079, blue: 0.079, alpha: 0.5)
    }
    
    override func calculatorTopBarTextColor() -> UIColor {
        return UIColor(red: 0.846, green: 0.846, blue: 0.846, alpha: 1)
    }
    
    override func calculatorAutocompleteBackgroundColor() -> UIColor {
        return .white
    }
    
    override func calculatorBottomSeparatorColor() -> UIColor {
        return UIColor(red: 0.846, green: 0.846, blue: 0.846, alpha: 1)
    }
    
    override func calculatorAutocompleteShadowColor() -> UIColor {
        return UIColor(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    
    override func calculatorAutocompleteBorderColor() -> UIColor {
        return UIColor(red: 0.95, green: 0.95, blue: 0.95, alpha: 1)
    }
    
    override func calculatorKeyboardColor() -> UIColor {
        return UIColor(red: 0.906, green: 0.906, blue: 0.906, alpha: 1)
    }
    
    override func calculatorAdditionalKeyboardActiveColor() -> UIColor {
        return UIColor(red: 0.855, green: 0.855, blue: 0.855, alpha: 1)
    }
    
    override func calculatorAnswerLabelCopyHighlighterColor() -> UIColor {
        return UIColor(red: 0.271, green: 0.545, blue: 0.192, alpha: 0.2)
    }
    
    override func rendererCalculationColor() -> UIColor {
        return UIColor(red: 0.271, green: 0.545, blue: 0.192, alpha: 1)
    }
    
    override func rendererCommentHighlightColor() -> UIColor {
        return UIColor(red: 22/255, green: 22/255, blue: 22/255, alpha: 0.25)
    }
    
    override func rendererCommentColor() -> UIColor {
        return UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1)
    }
    
    override func rendererTitleColor() -> UIColor {
        return UIColor(red: 194/255, green: 60/255, blue: 99/255, alpha: 1)
    }
    
    override func rendererUnitsColor() -> UIColor {
        return UIColor(red: 60/255, green: 145/255, blue: 190/255, alpha: 1)
    }
    
    override func rendererFunctionsColor() -> UIColor {
        return UIColor(red: 65/255, green: 111/255, blue: 197/255, alpha: 1)
    }
    
    override func rendererLinkBackgroundColor() -> UIColor {
        return UIColor(red: 0.925, green: 0.925, blue: 0.925, alpha: 1)
    }
    
    override func rendererLinkActiveBackgroundColor() -> UIColor {
        return UIColor(red: 0.746, green: 0.746, blue: 0.746, alpha: 1)
    }
    
    override func rendererLinkColor() -> UIColor {
        return UIColor(red: 0.079, green: 0.079, blue: 0.079, alpha: 1)
    }
    
    override func calculatorLinkButtonsTextColor() -> UIColor {
        return UIColor(red: 0.079, green: 0.079, blue: 0.079, alpha: 1)
    }
    
    override func calculatorLinkButtonsBackgroundColor() -> UIColor {
        return UIColor(red: 0.906, green: 0.906, blue: 0.906, alpha: 1)
    }
    
}
