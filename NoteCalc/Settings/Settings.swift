//
//  Settings.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 30.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kSettingsIcloudFailureTitle = NSLocalizedString("iCloud failure", comment: "")
let kSettingsIcloudFailureMessage = NSLocalizedString("Check if you are signed in iCloud in Settings", comment: "")
let kSettingsIcloudFailureClose = NSLocalizedString("Close", comment: "")

class Settings: NSObject {
    
    static public var shared = Settings()
    
    func select(_ group: String, value: String) {
        UserDefaults.standard.set(value, forKey: "settings\(group)")
        
        switch group {
            case "colorScheme":
                ColorScheme.shared.setColorScheme(value, animated: true)
            break
            default: break
        }
    }
    
    func switcher(_ group: String, value: Bool) -> Bool {
        UserDefaults.standard.set(value, forKey: "settings\(group)")
        
        switch group {
            case "iCloud":
                let sync = NotesFiles.shared.sync()
                
                if !sync {
                    UserDefaults.standard.set(!value, forKey: "settings\(group)")
                    
                    let alertController = UIAlertController(title: kSettingsIcloudFailureTitle, message: kSettingsIcloudFailureMessage, preferredStyle: .alert)
                    
//                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
//                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
//                            return
//                        }
//
//                        if UIApplication.shared.canOpenURL(settingsUrl) {
//                            UIApplication.shared.open(settingsUrl, completionHandler: { _ in })
//                        }
//                    }
//
//                    alertController.addAction(settingsAction)
                    
                    alertController.addAction(UIAlertAction(title: kSettingsIcloudFailureClose, style: .default))

                    UIApplication.shared.visibleViewController?.present(alertController, animated: true, completion: nil)
                }
                
                return sync
            default:
                return true
        }
    }
    
}
