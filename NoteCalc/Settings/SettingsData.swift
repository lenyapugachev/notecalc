//
//  SettingsData.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 13.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

let settingsKeyboardData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Use extra keyboard", comment: ""), "type": "Switch", "group": "additionalKeyboard", "delimTop": "full", "delimBottom": "full"]
]

let settingsColorSchemeData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Dark", comment: ""), "type": "Select", "group": "colorScheme", "value": "dark", "default": "1", "delimTop": "full", "delimBottom": "cut"],
    ["label": NSLocalizedString("Light", comment: ""), "type": "Select", "group": "colorScheme", "value": "light", "delimBottom": "full"]
]

let settingsTextFontData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("System", comment: ""), "type": "Select", "group": "font", "value": "system", "default": "1", "delimTop": "full", "delimBottom": "cut"],
    ["label": "Helvetica", "type": "Select", "group": "font", "value": "helvetica", "delimBottom": "cut"],
    ["label": "Lora", "type": "Select", "group": "font", "value": "lora", "delimBottom": "cut"],
    ["label": "IBM Plex Mono", "type": "Select", "group": "font", "value": "ibm", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Example:", comment: ""), "type": "Text"],
    ["label": NSLocalizedString("1290$ in euro + 358$ in euro", comment: ""), "type": "Example"]
]

let settingsTextSizeData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Small", comment: ""), "type": "Select", "group": "fontSize", "value": "small", "delimTop": "full", "delimBottom": "cut"],
    ["label": NSLocalizedString("Normal", comment: ""), "type": "Select", "group": "fontSize", "value": "normal", "default": "1", "delimBottom": "cut"],
    ["label": NSLocalizedString("Big", comment: ""), "type": "Select", "group": "fontSize", "value": "big", "delimBottom": "full"],
    ["type": "Separator"],
    ["label": NSLocalizedString("Example:", comment: ""), "type": "Text"],
    ["label": NSLocalizedString("1290$ in euro + 358$ in euro", comment: ""), "type": "Example"]
]

let settingsTextData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Font", comment: ""), "type": "Navigate", "value": "Font", "delimTop": "full", "delimBottom": "cut", "navigate": settingsTextFontData],
    ["label": NSLocalizedString("Font size", comment: ""), "type": "Navigate", "value": "FontSize", "delimBottom": "full", "navigate": settingsTextSizeData],
    ["type": "Separator"],
    ["label": NSLocalizedString("Page numbers will be displayed in the main interface. You can perform arithmetic operations with values that contain page numbers", comment: ""), "type": "Text"],
    ["label": NSLocalizedString("Show page numbers", comment: ""), "type": "Switch", "group": "pageNumbers", "delimTop": "full", "delimBottom": "Full"]
]

let settingsAnswerFormatData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Divide thousands", comment: ""), "type": "Switch", "group": "separateNumbers", "delimTop": "full", "delimBottom": "cut"],
    ["label": NSLocalizedString("Accuracy of response", comment: ""), "type": "Picker", "group": "answerPrecision", "delimBottom": "full"]
]

let settingsSyncData = [
    ["type": "Separator"],
    ["label": NSLocalizedString("Data will be synced via the selected service", comment: ""), "type": "Text"],
    ["label": NSLocalizedString("iCloud", comment: ""), "type": "Switch", "group": "iCloud", "delimTop": "full", "delimBottom": "Full"]
]

let settingsIndexData = [
    ["type": "Separator"],
    ["icon": "settingsKeyboard", "label": NSLocalizedString("Extra keyboard", comment: ""), "type": "Navigate", "delimTop": "full", "delimBottom": "cut", "navigate": settingsKeyboardData],
    ["icon": "settingsColorScheme", "label": NSLocalizedString("Color scheme", comment: ""), "type": "Navigate", "delimBottom": "full", "navigate": settingsColorSchemeData],
    ["type": "Separator"],
    ["icon": "settingsText", "label": NSLocalizedString("Text", comment: ""), "type": "Navigate", "delimTop": "full", "delimBottom": "cut", "navigate": settingsTextData],
    ["icon": "settingsNumberFormat", "label": NSLocalizedString("Response format", comment: ""), "type": "Navigate", "delimBottom": "full", "navigate": settingsAnswerFormatData],
    ["type": "Separator"],
    ["icon": "settingsSync", "label": NSLocalizedString("Synchronization", comment: ""), "type": "Navigate", "delimTop": "full", "delimBottom": "full", "navigate": settingsSyncData]
]
