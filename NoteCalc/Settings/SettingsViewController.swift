//
//  SettingsViewController.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 12.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kSettingsScreen = "Settings"
let kSettingsItemScreen = "SettingsItem"

let kSettingsStandartCellHeight: CGFloat = 44
let kSettingsSeparatorCellHeight: CGFloat = 32

class SettingsViewController: UIViewController {
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var back: UIButton!
    
    @IBOutlet weak var backgroundView: UIView!
            
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerViewTop: NSLayoutConstraint!
    
    // MARK: Actions
    
    @IBAction func onDismissTap(_ sender: Any) {
        dismiss(animated: true, completion: {
            if #available(iOS 13.0, *) {
                (UIApplication.shared.visibleViewController as? UIAdaptivePresentationControllerDelegate)?.presentationControllerDidDismiss?(self.presentationController!)
            } else {
                (UIApplication.shared.visibleViewController as? NotesViewController)?.updateLegacy()
            }
        })
    }
    
    @IBAction func onBackTap(_ sender: Any) {
        Router.shared.navigateBack(sender: self)
    }
    
    // MARK: Variables
    
    var settingsData: [[String:Any]] = settingsIndexData
    var header: String?
    
    // MARK: VC lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        headerView.layer.borderColor = kAppAccentBorderColor
        
        if header != nil {
            headerLabel.text = header
            back.isHidden = false
        }
        
        // MARK: Legacy popup
        
        if #available(iOS 13.0, *) {
            headerViewTop.constant = kAppHeaderViewTop
        } else {
            view.layer.cornerRadius = kAppLegacyPopupRadius
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ColorScheme.shared.controller = self
        
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    // MARK: Custom events
    
    @objc private func onCellTap(sender: UITapGestureRecognizer) {
        if let data = settingsData[sender.view!.tag]["navigate"] as? [[String:Any]] {
            Router.shared.navigateSettings(kSettingsItemScreen, sender: self, settingsData: data, settingsLabel: settingsData[sender.view!.tag]["label"] as! String)
        }
        
        if let group = settingsData[sender.view!.tag]["group"] as? String {
            if let value = settingsData[sender.view!.tag]["value"] as? String {
                Settings.shared.select(group, value: value)
                
                tableView.reloadData()
            }
        }
    }
    
}

// MARK: UITableView delegate

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = settingsData[indexPath.row]
        
        if let type = data["type"] as? String {
            if let cell = tableView.dequeueReusableCell(withIdentifier: type) {
                cell.selectionStyle = .none
                
                if let cell = cell as? KindaSettingsCellView {
                    cell.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
                    
                    cell.label.textColor = ColorScheme.shared.scheme.textColor()
                    cell.value?.textColor = ColorScheme.shared.scheme.textSecondaryColor()
                    
                    cell.arrowRight?.tintColor = ColorScheme.shared.scheme.foregroundColor()
                    
                    cell.topDelimeter?.backgroundColor = ColorScheme.shared.scheme.separatorColor()
                    cell.bottomDelimeter?.backgroundColor = ColorScheme.shared.scheme.separatorColor()
                    
                    if let icon = data["icon"] as? String {
                        cell.icon.image = UIImage(named: icon)
                        cell.icon.tintColor = ColorScheme.shared.scheme.foregroundColor()
                        
                        if icon == "settingsColorScheme" {
                            cell.icon.tintColor = ColorScheme.shared.scheme.foregroundColor().withAlphaComponent(0.5)
                        }
                    } else {
                        if cell.iconWidth != nil { cell.iconWidth.constant = 0 }
                    }
                    
                    cell.label.text = data["label"] as? String
                    
                    if cell.value != nil {
                        cell.value.text = data["value"] == nil ? "" : UserDefaults.standard.string(forKey: "settings\(data["value"] as! String)")
                    }
                    
                    switch type {
                        case "Text":
                            cell.backgroundColor = .clear
                            cell.label.textColor = ColorScheme.shared.scheme.textSecondaryColor()
                        break
                        case "Example":
                            cell.backgroundColor = .clear
                            cell.label.textColor = ColorScheme.shared.scheme.textSecondaryColor()
                            
                            cell.label.font = Renderer.shared.font()
                        break
                        case "Select":
                            cell.iconRight.isHidden = true
                            
                            if let group = data["group"] as? String {
                                if let savedValue = UserDefaults.standard.string(forKey: "settings\(group)") {
                                    if let value = data["value"] as? String {
                                        if savedValue == value {
                                            cell.iconRight.isHidden = false
                                        }
                                    }
                                } else {
                                    if let _ = data["default"] as? String {
                                        cell.iconRight.isHidden = false
                                    }
                                }
                            }
                        break
                        case "Switch":
                            if let group = data["group"] as? String {
                                let savedValue = UserDefaults.standard.bool(forKey: "settings\(group)")
                                
                                cell.switcher.isOn = savedValue
                                cell.switcherClosure = {
                                    let switched = Settings.shared.switcher(group, value: cell.switcher.isOn)
                                    
                                    if !switched { cell.switcher.isOn = !cell.switcher.isOn }
                                }
                            }
                        break
                        case "Picker":
                            if let group = data["group"] as? String {
                                let savedValue = UserDefaults.standard.integer(forKey: "settings\(group)")
                                
                                cell.value.text = "\(savedValue) \(kSettingsCellPickerSigns)"
                                
                                cell.picker.selectRow(savedValue, inComponent: 0, animated: false)
                                
                                Renderer.shared.clearCache()
                            }
                        break
                        default: break
                    }
                    
                    if cell.topDelimeter != nil {
                        cell.topDelimeter.isHidden = data["delimTop"] == nil
                        cell.topDelimeterLeft.constant = data["delimTop"] as? String == "cut" ? 10 : 0
                        
                        cell.bottomDelimeter.isHidden = data["delimBottom"] == nil
                        cell.bottomDelimeterLeft.constant = data["delimBottom"] as? String == "cut" ? 10 : 0
                    }
                    
                    cell.tag = indexPath.row
                    
                    cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onCellTap)))
                }
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch settingsData[indexPath.row]["type"] as! String {
            case "Navigate", "Switch", "Select": return kSettingsStandartCellHeight
            case "Separator": return kSettingsSeparatorCellHeight
            default: return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = kAppTapOpacity
        })
    }
    
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        UIView.animate(withDuration: kAppAnimationShort, animations: {
            self.tableView.cellForRow(at: indexPath)?.alpha = 1
        })
    }

}

// MARK: ColorScheme Delegate

extension SettingsViewController: ColorSchemeDelegate {
    
    func updateColorScheme(animated: Bool) {
        func set() {
            backgroundView.backgroundColor = ColorScheme.shared.scheme.backgroundMainColor()
            headerView.backgroundColor = ColorScheme.shared.scheme.backgroundPopupAdditionalColor()
            
            headerLabel.textColor = ColorScheme.shared.scheme.textColor()
            
            tableView.reloadData()
        }
        
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                set()
            })
        } else {
            set()
        }
    }
    
}
