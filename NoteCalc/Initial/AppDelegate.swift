//
//  AppDelegate.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 11.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

// MARK: Constants

let kAppAnimationShort = 0.2
let kAppAnimationStandart = 0.3
let kAppAnimationLong = 0.5
let kAppAnimationSuperLong = 1.0

let kAppTapOpacity: CGFloat = 0.7

let kAppSmallPhoneSize: CGFloat = 375.0
let kAppMediumPhoneSize: CGFloat = 414.0

let kAppHeaderViewTop: CGFloat = -1
let kAppLegacyPopupRadius: CGFloat = 12

let kAppWarningColor = UIColor(red: 0.765, green: 0.353, blue: 0.298, alpha: 1)
let kAppAccentBorderColor = UIColor(red: 0.329, green: 0.329, blue: 0.345, alpha: 0.65).cgColor

let kAppGeneratorLetters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var renderDataStocks: [String] = []

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let fileURL = URL(fileURLWithPath: "\(Bundle.main.resourcePath!)/RenderDataStocks.data")
            
        do {
            let stocks = try String(contentsOf: fileURL, encoding: .utf8)
            
            renderDataStocks = stocks.components(separatedBy: "\n")
        } catch {
            print(error.localizedDescription)
        }
        
        if !UserDefaults.standard.bool(forKey: "defaultSettings") {
            UserDefaults.standard.set(true, forKey: "defaultSettings")
            
            UserDefaults.standard.set(true, forKey: "settingsseparateNumbers")
            UserDefaults.standard.set(true, forKey: "settingsadditionalKeyboard")
            
            UserDefaults.standard.set(2, forKey: "settingsanswerPrecision")
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {}

}
