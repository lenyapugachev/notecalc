//
//  Meta.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 09.05.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

struct Meta {
    
    let label: String
    let imageUrl: URL?
    
}
