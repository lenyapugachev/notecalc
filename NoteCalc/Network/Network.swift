//
//  Network.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 23.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation
import Ocarina

// MARK: Constants

let kNetworkApiYahooHost = "apidojo-yahoo-finance-v1.p.rapidapi.com"
let kNetworkApiYahooUrl = "https://\(kNetworkApiYahooHost)/market/get-quotes?region=US&lang=en&symbols="
let kNetworkApiYahooKey = "fee0794210msh4d5bed5234e80aap1b4797jsn543a4d52be15"
let kNetworkApiYahooSeparator = "%252C"

let kNetworkCacheSeconds = 3600.0
let kNetworkTimeoutSeconds = 10.0

class Network: NSObject {
    
    static public var shared = Network()
    
    private var cache: [String:Double] = [:]
    private var cacheInvalidationTimers: [String:Timer] = [:]
    
    private var metaCache: [String:Meta] = [:]
    
    public func getStockUSDPrices(_ stocks: [String], completion: @escaping ([String:Double]) -> ()) {
        var result: [String:Double] = [:]
        
        for stock in stocks {
            if let cachedPrice = cache[stock] {
                result[stock] = cachedPrice
            }
        }
        
        if result.keys.count == stocks.count {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.02, execute: {
                completion(result)
            })
            
            return
        }
        
        get("\(kNetworkApiYahooUrl)\(stocks.joined(separator: kNetworkApiYahooSeparator))", headers:  [
            "x-rapidapi-host": kNetworkApiYahooHost,
            "x-rapidapi-key": kNetworkApiYahooKey
        ], completion: { data in
            var prices: [String:Double] = [:]

            if let stocks = (data?["quoteResponse"] as? [String:Any])?["result"] as? [NSDictionary] {
                for stock in stocks {
                    if let symbol = stock["symbol"] as? String {
                        if let price = stock["regularMarketPrice"] as? Double {
                            prices[symbol] = price
                            
                            self.cache[symbol] = price
                            
                            self.cacheInvalidationTimers[symbol]?.invalidate()
                            self.cacheInvalidationTimers[symbol] = Timer.scheduledTimer(withTimeInterval: kNetworkCacheSeconds, repeats: false, block: { _ in
                                self.cache.removeValue(forKey: symbol)
                            })
                        }
                    }
                }
            }

            completion(prices)
        })

//        DEBUG:
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3, execute: {
//            var fake: [String:Double] = [:]
//
//            for stock in stocks {
//                fake[stock] = 7.7
//            }
//
//            completion(fake)
//        })
    }
    
    public func getMetaInformation(_ url: String, completion: @escaping (Meta?) -> ()) {
        if let meta = metaCache[url] {
            return completion(meta)
        }
        
        URL(string: url)?.oca.fetchInformation(completionHandler: { (information, error) in
            if let information = information {
                self.metaCache[url] = Meta(label: information.title ?? url, imageUrl: information.imageURL)
                
                return completion(self.metaCache[url])
            } else if let _ = error {
                return completion(nil)
            }
        })
    }
    
    private func get(_ url: String, headers: [String:String] = [:], completion: @escaping (NSDictionary?) -> () = { _ in }, rawCompletion: ((String?) -> ())? = nil) {
        if let url = URL(string: url) {
            let request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: kNetworkTimeoutSeconds)
                    request.httpMethod = "GET"
                    request.allHTTPHeaderFields = headers

                    let session = URLSession.shared
                    let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            //            DEBUG:
            //            if let httpStatus = response as? HTTPURLResponse {
            //                print(httpStatus.statusCode)
            //            }
                        
                        if (error == nil) {
                            guard let data = data, error == nil else {
                                return
                            }

                            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                                return
                            }

                            do {
                                if rawCompletion != nil {
                                    rawCompletion!(String(data: data, encoding: .utf8))
                                } else {
                                    completion(try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary)
                                }
                            } catch let error as NSError {
                                print(error.localizedDescription)
                            }
                        }
                    })

                    dataTask.resume()
        } else {
            if rawCompletion != nil {
                rawCompletion!(nil)
            } else {
                completion(nil)
            }
        }
    }
    
}
