//
//  AdvancedButton.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 11.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

class AdvancedButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    override var isHighlighted: Bool {
        get {
            return super.isHighlighted
        }
        set {
            UIView.animate(withDuration: kAppAnimationShort, animations: {
                if newValue {
                    self.alpha = kAppTapOpacity
                } else {
                    self.alpha = 1
                }
            })
            
            super.isHighlighted = newValue
        }
    }
    
}

@objc class ClosureSleeve: NSObject {
    let closure: ()->()

    init (_ closure: @escaping ()->()) {
        self.closure = closure
    }

    @objc func invoke () {
        closure()
    }
}

extension UIControl {
    func addAction(for controlEvents: UIControl.Event = .touchUpInside, _ closure: @escaping ()->()) {
        let sleeve = ClosureSleeve(closure)
        addTarget(sleeve, action: #selector(ClosureSleeve.invoke), for: controlEvents)
        objc_setAssociatedObject(self, "[\(arc4random())]", sleeve, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
    }
}
