//
//  UITextViewFixed.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 16.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

@IBDesignable class UITextViewFixed: UITextView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textContainerInset = UIEdgeInsets.zero
        textContainer.lineFragmentPadding = 0
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        var caretRect = super.caretRect(for: position)
        
        caretRect.size.height = self.font!.pointSize + 5
        
        return caretRect
    }
    
    func currentWord(replace: String? = nil) -> (String,NSRange?) {
        guard let cursorRange = self.selectedTextRange else { return ("",nil) }
        
        func getRange(from position: UITextPosition, offset: Int) -> UITextRange? {
            guard let newPosition = self.position(from: position, offset: offset) else { return nil }
            return self.textRange(from: newPosition, to: position)
        }

        var wordStartPosition: UITextPosition = self.beginningOfDocument
        var wordEndPosition: UITextPosition = self.endOfDocument

        var position = cursorRange.start

        while let range = getRange(from: position, offset: -1), let text = self.text(in: range) {
            if text == " " || text == "\n" || text == " " {
                wordStartPosition = range.end
                break
            }
            position = range.start
        }

        position = cursorRange.start

        while let range = getRange(from: position, offset: 1), let text = self.text(in: range) {
            if text == " " || text == "\n" || text == " " {
                wordEndPosition = range.start
                
                break
            }
            
            position = range.end
        }

        guard let wordRange = self.textRange(from: wordStartPosition, to: wordEndPosition) else { return ("",nil) }
        
        if replace != nil { self.replace(wordRange, withText: replace!) }
        
        let location = offset(from: beginningOfDocument, to: wordRange.start)
        let length = offset(from: wordRange.start, to: wordRange.end)

        return (self.text(in: wordRange) ?? "", NSRange(location: location, length: length))
    }
    
}

extension UITextView {
    
    var nsRange: NSRange? {
        guard let range = selectedTextRange else { return nil }
        let location = offset(from: beginningOfDocument, to: range.start)
        let length = offset(from: range.start, to: range.end)
        return NSRange(location: location, length: length)
    }
    
}

extension NSRange {
    
    func toTextRange(textInput: UITextInput) -> UITextRange? {
        if let rangeStart = textInput.position(from: textInput.beginningOfDocument, offset: location), let rangeEnd = textInput.position(from: rangeStart, offset: length) {
            return textInput.textRange(from: rangeStart, to: rangeEnd)
        }
        
        return nil
    }
    
}
