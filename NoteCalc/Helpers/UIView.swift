//
//  UIView.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 26.04.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

extension UIView {
    
    private static let kRotationAnimationKey = "rotationanimationkey"

    func rotate(duration: Double = 2) {
        if layer.animation(forKey: UIView.kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")

            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float.pi * 2.0
            rotationAnimation.duration = duration
            rotationAnimation.repeatCount = Float.infinity

            layer.add(rotationAnimation, forKey: UIView.kRotationAnimationKey)
        }
    }

    func stopRotating() {
        if layer.animation(forKey: UIView.kRotationAnimationKey) != nil {
            layer.removeAnimation(forKey: UIView.kRotationAnimationKey)
        }
    }
    
}
