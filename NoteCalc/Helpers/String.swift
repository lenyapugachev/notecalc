//
//  String.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 16.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

extension String {
    
    var lines: [String] {
        return self.components(separatedBy: "\n")
    }
    
    func ranges(of substring: String, options: CompareOptions = [], locale: Locale? = nil) -> [Range<Index>] {
        var ranges: [Range<Index>] = []
        while ranges.last.map({ $0.upperBound < self.endIndex }) ?? true,
            let range = self.range(of: substring, options: options, range: (ranges.last?.upperBound ?? self.startIndex)..<self.endIndex, locale: locale)
        {
            ranges.append(range)
        }
        return ranges
    }
    
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
    
    func matches(withPattern pattern: String) -> [NSTextCheckingResult]? {
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        
        let nsrange = NSRange(startIndex..<endIndex, in: self)
        
        return regex?.matches(in: self, options: [], range: nsrange)
    }
    
    func replace(with: String = "", pattern: String) -> String {
        let regex = try? NSRegularExpression(pattern: pattern, options: [.caseInsensitive, .anchorsMatchLines])
        
        let nsrange = NSRange(startIndex..<endIndex, in: self)

        return regex?.stringByReplacingMatches(in: self, options: [], range: nsrange, withTemplate: with) ?? self
    }
    
    func indices(of string: String) -> [Int] {
        var indices = [Int]()
        var searchStartIndex = self.startIndex

        while searchStartIndex < self.endIndex,
            let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
            !range.isEmpty
        {
            let index = distance(from: self.startIndex, to: range.lowerBound)
            indices.append(index)
            searchStartIndex = range.upperBound
        }

        return indices
    }
    
    func rangeFromNSRange(nsRange : NSRange) -> Range<String.Index>? {
        return Range(nsRange, in: self)
    }
    
}
