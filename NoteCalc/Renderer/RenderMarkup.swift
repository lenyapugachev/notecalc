//
//  RenderMarkup.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 07.05.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

struct RenderMarkup {
    
    var range: NSRange
    var text: String
    
    let originalLocation: Int
    let originalLength: Int
    
    let type: String
    let data: String
    
}
