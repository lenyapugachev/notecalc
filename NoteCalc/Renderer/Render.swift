//
//  Render.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 16.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit

struct Render {
    
    let attributedText: NSAttributedString
    let rawText: String
    let calculationsView: UIView

}
