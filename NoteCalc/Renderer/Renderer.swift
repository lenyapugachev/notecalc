//
//  Renderer.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 16.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import UIKit
import SafariServices
import SGImageCache

// MARK: Constants

let kRendererParagraphStyleLineSpacing: CGFloat = 5
let kRendererParagraphStyleParagraphSpacing: CGFloat = 8

let kRendererFontSize: CGFloat = 17
let kRendererFontWeight = UIFont.Weight.medium

let kRendererCalculationHeight: CGFloat = 25

let kRendererBackgroundIdRegex = "~###([a-zA-Z0-9]*)###~"

class Renderer: NSObject {
    
    static public var shared = Renderer()
    static public var secondary: Renderer?
    
    // MARK: Variables
    
    public var calculatorController: CalculatorViewController?
    
    public var preparedData = RendererData()
    
    public var markupData: [NSRange:RenderMarkup] = [:]
    
    public var overrideFontDemo = false
    
    private var cache: [String:String] = [:]
    
    private var backgroundLabels: [String:([UILabel],String)] = [:]
    
    private var linkViews: [String:(UIView,UIImageView?,UILabel,String)] = [:]
    private var linkViewsMap: [NSRange:String] = [:]
    
    private var commentViews: [String:(UIView,String,String,NSRange)] = [:]
    
    private var em: Double = 16
    
    override init() {
        super.init()
        
        prepareData()
    }
    
    // MARK: Prepare Data
    
    private func prepareData() {
        for timezone in renderDataTimezones {
            preparedData.timezones[timezone.split(separator: "/").last!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "-", with: " ").lowercased()] = timezone
        }
        
        for key in TimeZone.abbreviationDictionary.keys {
            preparedData.timezones[key.lowercased()] = TimeZone.abbreviationDictionary[key]
        }
        
        preparedData.currenciesValues = Array(Set(renderDataCurrencies.values)).map { return $0.lowercased() }
        
        for currency in renderDataCurrencies.keys {
            preparedData.currenciesKey[currency.lowercased()] = renderDataCurrencies[currency]
        }
    }
    
    // MARK: Clear cache
    
    public func clearCache() {
        cache.removeAll()
        backgroundLabels.removeAll()
    }
    
    // MARK: Fix Markups
    
    public func fixMarkup(range: NSRange, text: String) {
        let length = text.utf16.count
        
        for key in markupData.keys {
            let keyEnd = key.location + key.length
            
            if range.location <= keyEnd {
                let value = markupData[key]
                
                markupData.removeValue(forKey: key)
                
                if range.length > 0 {
                    if range.location + range.length < keyEnd && key.location > range.location {
                        markupData[NSRange(location: key.location - range.length + length, length: key.length)] = value
                    }
                } else {
                    markupData[NSRange(location: key.location + (key.location > range.location ? length : 0), length: key.length)] = value
                }
            }
        }
    }
    
    private func loadMetaForLink(_ url: String, id: String) {
        linkViews[id]?.2.text = url
        
        Network.shared.getMetaInformation(url, completion: { meta in
            if let label = meta?.label {
                self.linkViews[id]?.2.text = label
            }
            
            if let url = meta?.imageUrl?.absoluteString {
                if let image = SGImageCache.image(forURL: url) {
                    self.linkViews[id]?.1?.contentMode = .scaleAspectFill
                    self.linkViews[id]?.1?.image = image
                } else {
                    SGImageCache.getImage(url: url) { [weak self] image in
                        self?.linkViews[id]?.1?.contentMode = .scaleAspectFill
                        self?.linkViews[id]?.1?.image = image
                    }
                }
            }
        })
    }
    
    public func highlightMarkupAtRange(_ range: NSRange) {
        if let range = self.linkViewsMap[range], let link = self.linkViews[range] {
            UIView.animate(withDuration: 0.2, animations: {
                for link in self.linkViews.keys {
                    self.linkViews[link]?.0.backgroundColor = ColorScheme.shared.scheme.rendererLinkBackgroundColor()
                }
                
                link.0.backgroundColor = ColorScheme.shared.scheme.rendererLinkActiveBackgroundColor()
            })
        }
    }
    
    public func textViewTap(_ location: CGPoint) {
        for key in linkViews.keys {
            let linkView = linkViews[key]!
            
            let located = linkView.0.frame.contains(location)
            
            if located {
                if let url = URL(string: linkView.3) {
                    if #available(iOS 11.0, *) {
                        let config = SFSafariViewController.Configuration()

                        let vc = SFSafariViewController(url: url, configuration: config)
                        
                        UIApplication.shared.visibleViewController?.present(vc, animated: true)
                    } else {
                        UIApplication.shared.open(url)
                    }
                }
            }
        }
        
        for key in commentViews.keys {
            let commentView = commentViews[key]!
            
            let located = commentView.0.frame.contains(location)
            
            if located {
                (UIApplication.shared.visibleViewController as? CalculatorViewController)?.viewComment(commentView)
            }
        }
    }
    
    // MARK: Render
    
    public func render(_ string: String, frameSizeWidth: CGFloat) -> Render {
        // MARK: Basic Font & Paragraph
        
        let font = self.font()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
        paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing
        
        func getAttributedString(_ string: String) -> NSMutableAttributedString {
            return NSMutableAttributedString(string: string, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: ColorScheme.shared.scheme.textColor(), NSAttributedString.Key.paragraphStyle: paragraphStyle])
        }
        
        // MARK: Parse
        
        var rawString = string
        
        let calculationsView = UIView(frame: CGRect(x: 0, y: 0, width: (frameSizeWidth - kCalculatorTextViewMargin) * 2, height: 0))
        
        var variables: [String:String] = [:]
        var variablesMap: [String] = []
        
        let fontFont = UserDefaults.standard.string(forKey: "settingsfont") ?? "system"
        let fontSize = UserDefaults.standard.string(forKey: "settingsfontSize") ?? "normal"
        
        var calculationHeight: CGFloat = 0
        
        switch fontFont {
            case "helvetica":
                calculationHeight = fontSize == "big" ? 0 : (fontSize == "small" ? -3 : -1)
            break
            case "lora":
                calculationHeight = fontSize == "big" ? 0 : (fontSize == "small" ? -4 : -2)
            break
            case "ibm":
                calculationHeight = fontSize == "big" ? 1 : (fontSize == "small" ? -2.5 : -1)
            break
            default:
                calculationHeight = fontSize == "big" ? 0 : (fontSize == "small" ? -4 : -2)
            break
        }
        
        // MARK: Resolve data
        
        var rawData = ""
        
        let parts = string.components(separatedBy: "///DATA///")
        
        if parts.count > 1 {
            rawString = parts[0]
            
            rawData = parts[1]
            
            let parts = rawData.split(separator: "⚛︎")
            
            for part in parts {
                let parts = part.split(separator: "☯︎")
                
                let range = NSRange(location: Int(parts[3])!, length: Int(parts[4])!)
                
                markupData[range] = RenderMarkup(range: range, text: String(parts[0]), originalLocation: range.location, originalLength: range.length, type: String(parts[1]), data: String(parts[2]))
            }
        }
        
        if rawData.count == 0 {
            for key in markupData.keys {
                if let markup = markupData[key] {
                    rawData += (rawData == "" ? "" : "⚛︎") + "\(markup.text)☯︎\(markup.type)☯︎\(markup.data)☯︎\(key.location)☯︎\(key.length)"
                }
            }
        }
        
        rawString = rawString.replacingOccurrences(of: "///DATA///", with: "")
        
        // MARK: Mark up text
        
        let attributedText = getAttributedString(rawString)
        
        var links: [Int:RenderMarkup] = [:]
        var comments: [Int:[RenderMarkup]] = [:]
        
        for key in markupData.keys {
            if var markup = markupData[key] {
                switch markup.type {
                    case "comment":
                        let lineString = String(rawString[String.Index(utf16Offset: 0, in: rawString)..<String.Index(utf16Offset: key.location, in: rawString)])
                        let components = lineString.components(separatedBy: "\n")
                        let line = components.count
                        
                        if let last = components.last?.count {
                            markup.range = NSRange(location: last, length: markup.range.length)
                            
                            if comments[line] != nil {
                                comments[line]?.append(markup)
                            } else {
                                comments[line] = [markup]
                            }
                        }
                    break
                    case "link":
                        switch markup.data {
                            case "simple":
                                attributedText.addAttribute(.link, value: markup.text, range: NSRange(location: key.location, length: key.length))
                            break
                            case "short":
                                let lineString = String(rawString[String.Index(utf16Offset: 0, in: rawString)..<String.Index(utf16Offset: key.location, in: rawString)])
                                let line = lineString.components(separatedBy: "\n").count
                                
                                links[line] = markup
                                
                                let range = NSRange(location: key.location, length: key.length)
                                
                                attributedText.addAttribute(.kern, value: -20, range: range)
                            break
                            case "full":
                                let lineString = String(rawString[String.Index(utf16Offset: 0, in: rawString)..<String.Index(utf16Offset: key.location + key.length, in: rawString)])
                                let line = lineString.components(separatedBy: "\n").count
                                 
                                links[line] = markup
                                
                                let paragraphStyle = NSMutableParagraphStyle()
                                paragraphStyle.lineSpacing = 64
                                
                                let range = NSRange(location: key.location, length: key.length)
                                
                                attributedText.addAttribute(.paragraphStyle, value: paragraphStyle, range: range)
                                attributedText.addAttribute(.kern, value: -20, range: range)
                            break
                            default: break
                        }
                    break
                    default: break
                }
            }
        }
        
        for unit in renderDataUnits.keys {
            markUp(rawString, pattern: "\\b ?\(unit) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for unit in renderDataUnitsBytes.keys {
            markUp(rawString, pattern: "\\b ?\(unit) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText, caseInsensitive: false)
        }
        
        for unit in renderDataSoloUnits.keys {
            markUp(rawString, pattern: "(\\b| )[0-9]{1,}(\(unit))($|\n| )", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        markUp(rawString, pattern: "%", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        
        markUp(rawString, pattern: kRenderExpressionTimeWord, color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        markUp(rawString, pattern: kRenderExpressionTimeRuWord, color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        markUp(rawString, pattern: kRenderExpressionTimeWordAlt, color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        markUp(rawString, pattern: kRenderExpressionTimeRuWordAlt, color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        
        for timezone in renderDataTimezones {
            markUp(rawString, pattern: "\\b\(timezone)\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
            markUp(rawString, pattern: "\\b\(timezone.split(separator: "/").last!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "+", with: #"\+"#))\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for timezone in TimeZone.abbreviationDictionary.keys {
            markUp(rawString, pattern: "\\b\(timezone)\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for unit in renderDataOperators.keys {
            markUp(rawString, pattern: "\\b ?\(unit) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for unit in renderDataSoloOperators {
            markUp(rawString, pattern: "\(unit)", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for symbol in renderDataCurrenciesSymbols.keys {
            markUp(rawString, pattern: "\\\(symbol)", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        let stocks = renderDataStocks.flatMap { $0.components(separatedBy: " ")}
        for stock in stocks.filter({ rawString.uppercased().range(of: $0) != nil }) {
            markUp(rawString, pattern: "\\b ?\(stock) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }

        for currency in renderDataCurrencies.keys {
            markUp(rawString, pattern: "\\b ?\(currency) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }

        for currency in Array(Set(renderDataCurrencies.values)) {
            markUp(rawString, pattern: "\\b ?\(currency) ?\\b", color: ColorScheme.shared.scheme.rendererUnitsColor(), attributedText: attributedText)
        }
        
        for constant in renderDataConstants {
            markUp(rawString, pattern: "\\b ?\(constant) ?\\b", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText)
        }
        
        for variable in renderDataVariables.keys {
            markUp(rawString, pattern: "\\b ?\(variable) ?\\b", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText)
        }
        
        for function in renderDataFunctions.keys {
            markUp(rawString, pattern: "\\b ?\(function) ?\\b", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText)
        }
        
        for function in renderDataSoloFunctions.keys {
            markUp(rawString, pattern: "\(function)", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText)
        }
        
        markUp(rawString, pattern: #"^(.*) = (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, _ in
                self.markUp(rawString, pattern: "\\b ?\(variable) ?\\b", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText)
        }
        
        markUp(rawString, pattern: #"^(.*) \+= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { _, _ in }
        markUp(rawString, pattern: #"^(.*) \-= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { _, _ in }
        markUp(rawString, pattern: #"^(.*) \*= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { _, _ in }
        markUp(rawString, pattern: #"^(.*) \/= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { _, _ in }
        
        // MARK: Calculation results
        
        func pushVariable(_ variable: String) {
            if let index = variablesMap.firstIndex(of: variable) {
                variablesMap.remove(at: index)
            }
            
            variablesMap.append(variable)
            
            if let index = variablesMap.firstIndex(of: variables[variable]!) {
                variables[variable] = variables[variablesMap[index]]!
            } else {
                if let index = renderDataVariables.keys.firstIndex(of: variables[variable]!) {
                    if let value = variables[renderDataVariables[renderDataVariables.keys[index]] ?? ""] {
                        variables[variable] = value
                    }
                }
            }
        }
        
        var section: [String] = []
        
        linkViews.removeAll()
        linkViewsMap.removeAll()
        
        commentViews.removeAll()
        
        em = 16
        
        for (i,line) in rawString.lines.enumerated() {
            var doNotCache = false
            
            let line = line.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if line.count == 0 { section.removeAll() }
            
            var lineToCalculate = line
            
            variables["total"] = calculateLine(section.joined(separator: " + "))?.0 ?? ""
            pushVariable("total")
            
            variables["average"] = calculateLine("avg " + section.joined(separator: "|"))?.0 ?? ""
            pushVariable("average")
            
            variables["min"] = calculateLine("min " + section.joined(separator: "|"))?.0 ?? ""
            pushVariable("min")
            
            variables["max"] = calculateLine("max " + section.joined(separator: "|"))?.0 ?? ""
            pushVariable("max")
            
            if let match = line.matches(withPattern: "em = ([0-9]*) px") {
                if let match = match.first, let wordRange = line.rangeFromNSRange(nsRange: match.range(at: 1)) {
                    if let double = Double(line[wordRange]) {
                        self.em = double
                    }
                }
            }
            
            markUp(line, justSearch: true, pattern: #"^(.*) = (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, expression in
                if variable != "em" {
                    variables[variable] = expression
                    
                    pushVariable(variable)
                }
            }

            markUp(line, justSearch: true, pattern: #"^(.*) \+= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, expression in
                if let mutated = self.mutateVariable(variables: variables, variable: variable, expression: expression, operation: "+") {
                    variables[variable] = mutated
                    
                    pushVariable(variable)
                }
            }

            markUp(line, justSearch: true, pattern: #"^(.*) \-= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, expression in
                if let mutated = self.mutateVariable(variables: variables, variable: variable, expression: expression, operation: "-") {
                    variables[variable] = mutated
                    
                    pushVariable(variable)
                }
            }

            markUp(line, justSearch: true, pattern: #"^(.*) \*= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, expression in
                if let mutated = self.mutateVariable(variables: variables, variable: variable, expression: expression, operation: "*") {
                    variables[variable] = mutated
                    
                    pushVariable(variable)
                }
            }

            markUp(line, justSearch: true, pattern: #"^(.*) \/= (.*)"#, color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { variable, expression in
                if let mutated = self.mutateVariable(variables: variables, variable: variable, expression: expression, operation: "/") {
                    variables[variable] = mutated
                    
                    pushVariable(variable)
                }
            }
            
            for variable in variablesMap.reversed() {
                markUp(rawString, pattern: "\\b ?\(variable) ?\\b", color: ColorScheme.shared.scheme.rendererFunctionsColor(), attributedText: attributedText) { _, _ in }
            }
            
            for variable in variablesMap.reversed() {
                for key in renderDataVariables.keys {
                    if variable == renderDataVariables[key]! {
                        if lineToCalculate.ranges(of: " " + key + " ").count > 0 {
                            doNotCache = true
                        }
                        
                        lineToCalculate = lineToCalculate.replacingOccurrences(of: " " + key + " ", with: " " + variables[renderDataVariables[key]!]! + " ", options: [.caseInsensitive])
                        lineToCalculate = lineToCalculate.replacingOccurrences(of: " " + key + " ", with: " " + variables[renderDataVariables[key]!]! + " ", options: [.caseInsensitive])
                        
                        if lineToCalculate.hasSuffix(key) {
                            let mutated = lineToCalculate.count > key.count ? String(lineToCalculate[lineToCalculate.index(lineToCalculate.startIndex, offsetBy: 0)...lineToCalculate.index(lineToCalculate.endIndex, offsetBy: -key.count - 1)]) : ""
                            
                            if lineToCalculate.count == key.count || mutated.last == " " {
                                doNotCache = true
                                
                                lineToCalculate = mutated + variables[renderDataVariables[key]!]!
                            }
                        }
                        
                        if lineToCalculate.hasPrefix(key) {
                            let mutated = String(lineToCalculate[lineToCalculate.index(lineToCalculate.startIndex, offsetBy: key.count)...lineToCalculate.index(lineToCalculate.endIndex, offsetBy: -1)])
                            
                            if lineToCalculate.count == key.count || mutated.first == " " {
                                doNotCache = true
                                
                                lineToCalculate = variables[renderDataVariables[key]!]! + (lineToCalculate.count > key.count ? mutated : "")
                            }
                        }
                    }
                }
                
                if lineToCalculate.ranges(of: " " + variable + " ").count > 0 || lineToCalculate.ranges(of: " em ").count > 0 {
                    doNotCache = true
                }
                
                lineToCalculate = lineToCalculate.replacingOccurrences(of: " " + variable + " ", with: " " + variables[variable]! + " ", options: [.caseInsensitive])
                lineToCalculate = lineToCalculate.replacingOccurrences(of: " " + variable + " ", with: " " + variables[variable]! + " ", options: [.caseInsensitive])
                
                if lineToCalculate.hasSuffix(variable) {
                    let mutated = lineToCalculate.count > variable.count ? String(lineToCalculate[lineToCalculate.index(lineToCalculate.startIndex, offsetBy: 0)...lineToCalculate.index(lineToCalculate.endIndex, offsetBy: -variable.count - 1)]) : ""
                    
                    if lineToCalculate.count == variable.count || mutated.last == " " {
                        doNotCache = true
                        
                        lineToCalculate = mutated + variables[variable]!
                    }
                }
                
                if lineToCalculate.hasPrefix(variable) {
                    let mutated = String(lineToCalculate[lineToCalculate.index(lineToCalculate.startIndex, offsetBy: variable.count)...lineToCalculate.index(lineToCalculate.endIndex, offsetBy: -1)])
                    
                    if lineToCalculate.count == variable.count || mutated.first == " " {
                        doNotCache = true
                        
                        lineToCalculate = variables[variable]! + (lineToCalculate.count > variable.count ? mutated : "")
                    }
                }
            }
            
            let result = calculateLine(lineToCalculate, user: true)
            
            var label: UILabel!
            var imageView: UIImageView!
            
            if result != nil {
                label = UILabel(frame: CGRect(x: frameSizeWidth, y: calculationHeight, width: frameSizeWidth - kCalculatorTextViewMargin * 2, height: kRendererCalculationHeight))
                
                label.tag = i
                label.numberOfLines = 1
                label.adjustsFontSizeToFitWidth = true
                label.font = font
                label.textColor = ColorScheme.shared.scheme.rendererCalculationColor()
                label.textAlignment = .right
//                label.backgroundColor = UIColor.cyan.withAlphaComponent(0.4)
                
                calculationsView.addSubview(label)
                
                variables["prev"] = result!.0!
                pushVariable("prev")

                if result!.0!.matches(kRendererBackgroundIdRegex) {
                    if var labels = backgroundLabels[result!.0!]?.0 {
                        labels.append(label)
                        backgroundLabels[result!.0!] = (labels,line)
                    } else {
                        backgroundLabels[result!.0!] = ([label],line)
                    }
                    
                    expireBackgroundLabel(result!.0!)
                    
                    if let image = UIImage(named: "calculatorAnswerLoader") {
                        imageView = UIImageView(image: image)
                        
                        imageView.frame = CGRect(x: label.frame.width - kCalculatorTextViewMargin / 2, y: 0, width: kRendererCalculationHeight, height: kRendererCalculationHeight)
                        imageView.contentMode = .center
                        imageView.rotate()
                        
                        label.addSubview(imageView)
                    }
                } else {
                    var answer = result!.0!
                    
                    for key in renderDataUnitsSymbols.keys.sorted(by: { $0.components(separatedBy: " ").count > $1.components(separatedBy: " ").count }) {
                        answer = answer.replace(with: " \(renderDataUnitsSymbols[key]!)", pattern: " \(key)$")
                    }
                    
                    label.text = answer
                    
                    if !doNotCache && result!.1 > 0 {
                        cache[line] = result!.0!
                        
                        expireCache(line, result!.1)
                    }
                    
                    section.append(result!.0!)
                }
            } //else {
//                markUp(rawString, pattern: line, color: UIColor.yellow, attributedText: attributedText)
//            }
            
            if UserDefaults.standard.bool(forKey: "settingspageNumbers") {
                let number = UILabel(frame: CGRect(x: -18, y: calculationHeight, width: 14, height: kRendererCalculationHeight))
                
                number.numberOfLines = 1
                number.adjustsFontSizeToFitWidth = true
                number.font = .systemFont(ofSize: 10, weight: .medium)
                number.textColor = ColorScheme.shared.scheme.rendererCommentColor()
                number.textAlignment = .center
                number.text = "\(i + 1)"
//                number.backgroundColor = UIColor.red.withAlphaComponent(0.4)
                
                calculationsView.addSubview(number)
            }
            
            // MARK: Checking for Markup
            
            let link = links[i + 1]
            
            if link != nil {
                let linkView = UIView(frame: CGRect(x: 0, y: calculationHeight, width: frameSizeWidth, height: 24))
                
                linkView.backgroundColor = ColorScheme.shared.scheme.rendererLinkBackgroundColor()
                linkView.layer.cornerRadius = 3
                
                let label = UILabel(frame: CGRect(x: 7, y: 0, width: frameSizeWidth - 14, height: 24))
                
                label.text = link!.text
                label.font = .systemFont(ofSize: 16, weight: .medium)
                
                linkView.addSubview(label)
                
                var image: UIImageView? = nil
                
                if link?.data == "full" {
                    linkView.frame.size.height += 50
                    
                    label.frame.size.width -= 70
                    label.frame.size.height *= 2
                    
                    label.frame.origin.x = 75
                    label.frame.origin.y = 12
                    
                    label.numberOfLines = 2
                    
                    image = UIImageView(frame: CGRect(x: 7, y: 7, width: 60, height: 60))
                    
                    image?.image = UIImage(named: "logo")
                    image?.backgroundColor = .white
                    image?.contentMode = .center
                    image?.tintColor = .black
                    image?.clipsToBounds = true
                    
                    linkView.addSubview(image!)
                }
                
                calculationsView.addSubview(linkView)
                
                let id = String((0..<24).map { _ in kAppGeneratorLetters.randomElement()! })
                
                linkViews[id] = (linkView,image,label,link!.text)
                linkViewsMap[link!.range] = id
                
                loadMetaForLink(link!.text, id: id)
            }
            
            if let array = comments[i + 1] {
                for comment in array {
                    var comment = comment
                    
                    if line.count - comment.range.location < comment.range.length {
                        let range = NSRange(location: 0, length: comment.range.length - (line.count - comment.range.location))
                        
                        let nextMarkup = RenderMarkup(range: range, text: ">", originalLocation: comment.originalLocation, originalLength: comment.originalLength, type: comment.type, data: comment.data)
                        
                        if comments[i + 2] != nil {
                            comments[i + 2]?.append(nextMarkup)
                        } else {
                            comments[i + 2] = [nextMarkup]
                        }
                        
                        comment.range.length = line.count - comment.range.location
                        
                        switch comment.text {
                            case ".":
                                comment.text = "<"
                            break
                            case ">":
                                comment.text = "-"
                            break
                            default: break
                        }
                    }
                    
                    let fakeLabel = UILabel()
                    
                    fakeLabel.font = self.font()
                    
                    let commentedLine = line[String.Index(utf16Offset: comment.range.lowerBound, in: line)..<String.Index(utf16Offset: comment.range.upperBound, in: line)]
                    
                    fakeLabel.attributedText = getAttributedString(String(commentedLine))
                    
                    let width = fakeLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: kRendererCalculationHeight)).width
                    
                    var x: CGFloat = 0
                    
                    if comment.range.location > 0 {
                        let beforeLine = line[String.Index(utf16Offset: 0, in: line)..<String.Index(utf16Offset: comment.range.lowerBound, in: line)]
                        
                        fakeLabel.attributedText = getAttributedString(String(beforeLine))
                        
                        x = fakeLabel.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: kRendererCalculationHeight)).width
                    }
                    
                    let commentHighlighter = UIView(frame: CGRect(x: x - 1, y: calculationHeight, width: width + 2, height: kRendererCalculationHeight))
                    
                    commentHighlighter.backgroundColor = ColorScheme.shared.scheme.rendererCommentHighlightColor()
                    commentHighlighter.layer.cornerRadius = 2
                    
                    calculationsView.addSubview(commentHighlighter)
                    
                    let id = String((0..<24).map { _ in kAppGeneratorLetters.randomElement()! })
                    
                    commentViews[id] = (commentHighlighter,comment.data,String(commentedLine),NSRange(location: comment.originalLocation, length: comment.originalLength))
                }
            }
            
            // MARK: Add some height
            
            let fakeLabel = UILabel()
            
            fakeLabel.font = self.font()
            fakeLabel.numberOfLines = 0
            
            fakeLabel.attributedText = getAttributedString(link != nil ? line.replacingOccurrences(of: link!.text, with: "N") : line)
            
            let height = fakeLabel.sizeThatFits(CGSize(width: frameSizeWidth, height: CGFloat.greatestFiniteMagnitude)).height
            
            let baseline = fakeLabel.font.lineHeight + kRendererParagraphStyleLineSpacing
            
            let lines = round((height > 0 ? height : baseline) / baseline)
            
            calculationHeight += lines * fakeLabel.font.lineHeight + lines * kRendererParagraphStyleLineSpacing + kRendererParagraphStyleParagraphSpacing + (link != nil ? (link!.data == "full" ? 50 : 0) : 0)
            
            label?.frame.origin.y += (lines - 1) * fakeLabel.font.lineHeight + (lines - 1) * kRendererParagraphStyleLineSpacing + (link != nil ? (link!.data == "full" ? 50 : 0) : 0)
        }
        
        markUp(rawString, pattern: #"\/\/.*"#, color: ColorScheme.shared.scheme.rendererCommentColor(), attributedText: attributedText)
        
        markUp(rawString, pattern: "(https?:[A-Z0-9a-z\\.\\/_-]*)(\\?)?([a-zA-Z0-9@;:%_\\+.~#?&\\/=-−-]*)?", underlineColor: ColorScheme.shared.scheme.rendererCommentColor(), attributedText: attributedText)
        
        markUp(rawString, pattern: #"^(.*:) .*"#, color: ColorScheme.shared.scheme.rendererTitleColor(), attributedText: attributedText)
        
        for key in markupData.keys {
            let range = NSRange(location: key.location, length: key.length)
            
            if let markup = markupData[key] {
                switch markup.type {
                    case "link":
                        switch markup.data {
                            case "short", "full":
                                attributedText.addAttribute(.foregroundColor, value: UIColor.clear, range: range)
                                attributedText.addAttribute(.underlineColor, value: UIColor.clear, range: range)
                            break
                            default: break
                        }
                    break
                    default: break
                }
            }
        }
        
        return Render(attributedText: attributedText, rawText: "\(rawString)///DATA///\(rawData)", calculationsView: calculationsView)
    }
    
    private func mutateVariable(variables: [String:String], variable: String, expression: String, operation: String) -> String? {
        if let exist = variables[variable] {
            return "(\(exist) \(operation) \(expression))"
        }
        
        if operation == "+" {
            return "\(expression)"
        }
        
        if operation == "-" {
            return "-\(expression)"
        }
        
        return nil
    }
    
    // MARK: Font
    
    public func font() -> UIFont {
        let fontFont = overrideFontDemo ? "ibm" : (UserDefaults.standard.string(forKey: "settingsfont") ?? "system")
        let fontSize = overrideFontDemo ? "normal" : (UserDefaults.standard.string(forKey: "settingsfontSize") ?? "normal")
        
        let fontPoints: CGFloat = fontSize == "big" ? 20 : (fontSize == "small" ? 14 : 17)
        
        let font = UIFont.systemFont(ofSize: fontPoints, weight: .medium)

        switch fontFont {
            case "helvetica":
                return UIFont(name: "HelveticaNeue-Medium", size: fontPoints) ?? font
            case "lora":
                return UIFont(name: "Lora-Regular", size: fontPoints) ?? font
            case "ibm":
                return UIFont(name: "IBMPlexMono-Medium", size: fontPoints) ?? font
            default:
                return font
        }
    }
    
    // MARK: Get text for autocomplete
    
    public func autocompleteText(_ key: String, type: String, currentWord: String) -> NSAttributedString? {
        let font = UIFont.systemFont(ofSize: kRendererFontSize, weight: kRendererFontWeight)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = kRendererParagraphStyleLineSpacing
        paragraphStyle.paragraphSpacing = kRendererParagraphStyleParagraphSpacing
        
        var color = ColorScheme.shared.scheme.rendererUnitsColor()
        
        switch type {
            case "Function":
                color = ColorScheme.shared.scheme.rendererFunctionsColor()
            break
            default: break
        }
        
        let string = NSMutableAttributedString(string: key, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color.withAlphaComponent(0.5), NSAttributedString.Key.paragraphStyle: paragraphStyle])
        
        markUp(key, pattern: "\(currentWord)", color: color, attributedText: string)
        
        return string
    }
    
    // MARK: Fill background operation result
    
    public func fillBackgroundId(_ id: String, value: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.04, execute: {
            for key in self.backgroundLabels.keys {
                let newKey = key.replace(with: value, pattern: "~###\(id)###~")
                
                if !newKey.matches(kRendererBackgroundIdRegex) {
                    if let result = RenderExpression(newKey, rendererPreparedData: self.preparedData).solve() {
                        if let label = self.backgroundLabels[key] {
                            let line = label.1.trimmingCharacters(in: .whitespacesAndNewlines)
                            
                            if !result.0!.matches(kRendererBackgroundIdRegex) {
                                var answer = result.0!
                                
                                for key in renderDataUnitsSymbols.keys {
                                    answer = answer.replace(with: " \(renderDataUnitsSymbols[key]!)", pattern: " \(key)$")
                                }
                                
                                DispatchQueue.main.async {
                                    for label in label.0 {
                                        label.text = answer
                                        
                                        label.subviews.first?.removeFromSuperview()
                                    }
                                    
                                    if label.0.first!.tag == 777 {
                                        if let id = UserDefaults.standard.string(forKey: kNotesDefaultsNoteId) {
                                            UserDefaults.standard.set(answer, forKey: kNotesDefaultsNoteSum + id)
                                        }
                                    } else {
                                        self.calculatorController?.solveSum()
                                    }
                                }
                                
                                if result.1 > 0 {
                                    self.cache[line] = result.0
                                    
                                    self.expireCache(line, result.1)
                                    
                                    self.backgroundLabels.removeValue(forKey: key)
                                }
                            } else {
                                self.backgroundLabels[result.0!] = label
                                
                                self.expireBackgroundLabel(result.0!)

                                self.backgroundLabels.removeValue(forKey: key)
                            }
                        }
                    }
                } else {
                    if key != newKey {
                        self.backgroundLabels[newKey] = self.backgroundLabels[key]
                        
                        self.expireBackgroundLabel(newKey)
                        
                        self.backgroundLabels.removeValue(forKey: key)
                    }
                }
            }
        })
    }
    
    // MARK: Solve sum
    
    public func solveSum(_ expression: String, label: UILabel) -> String? {
        if var answer = RenderExpression(expression, rendererPreparedData: preparedData).solve()?.0 {
            if answer.matches(kRendererBackgroundIdRegex) {
                label.text = ""
                
                backgroundLabels[answer] = ([label], expression)
            } else {
                for key in renderDataUnitsSymbols.keys.sorted(by: { $0.components(separatedBy: " ").count > $1.components(separatedBy: " ").count }) {
                    answer = answer.replace(with: " \(renderDataUnitsSymbols[key]!)", pattern: " \(key)$")
                }
                
                label.text = answer
                
                return answer
            }
        } else {
            label.text = ""
        }
        
        return nil
    }
    
    // MARK: Expire cache and background label
    
    private func expireCache(_ key: String, _ seconds: Int) {
        DispatchQueue.main.asyncAfter(deadline: .now() + TimeInterval(seconds), execute: {
            self.cache.removeValue(forKey: key)
        })
    }
    
    private func expireBackgroundLabel(_ id: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 10, execute: {
            if let matches = self.backgroundLabels[id]?.0.first?.text?.matches(kRendererBackgroundIdRegex) {
                if matches {
                    if let labels = self.backgroundLabels[id]?.0 {
                        for label in labels {
                            label.removeFromSuperview()
                        }
                    }
                    
                    self.backgroundLabels.removeValue(forKey: id)
                }
            }
        })
    }
    
    // MARK: Calculate line
    
    private func calculateLine(_ line: String, user: Bool = false) -> (String?,Int)? {
        if line.count == 0 { return nil }
        
        if cache[line] != nil {
            return (cache[line], 0)
        }
        
        return RenderExpression(line, rendererPreparedData: preparedData, user: user, em: em).solve()
    }
    
    // MARK: Mark up part of text

    private func markUp(_ string: String, justSearch: Bool = false, pattern: String, color: UIColor = ColorScheme.shared.scheme.textColor(), underlineColor: UIColor? = nil, attributedText: NSMutableAttributedString, caseInsensitive: Bool = true, variable: ((String,String) -> ())? = nil) {
        let regex = try? NSRegularExpression(pattern: pattern, options: caseInsensitive ? [.caseInsensitive, .anchorsMatchLines] : [.anchorsMatchLines])
        
        let nsrange = NSRange(string.startIndex..<string.endIndex, in: string)
        
        regex?.enumerateMatches(in: string, options: [], range: nsrange) { (match, _, stop) in
            guard let match = match else { return }
            
            if match.numberOfRanges == 1 {
                if !justSearch {
                    attributedText.addAttribute(.foregroundColor, value: color, range: match.range(at: 0))
                    
                    if underlineColor != nil {
                        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: match.range(at: 0))
                        attributedText.addAttribute(.underlineColor, value: underlineColor!, range: match.range(at: 0))
                    }
                }
                
                return
            }
            
            var variableName = ""
            
            for index in 1...(match.numberOfRanges - 1) {
                if variable != nil {
                    if let s = string.index(string.startIndex, offsetBy: match.range(at: index).lowerBound, limitedBy: string.endIndex),
                       let e = string.index(string.startIndex, offsetBy: match.range(at: index).upperBound, limitedBy: string.endIndex) {
                    
                        if match.numberOfRanges - 1 != index {
                            if !justSearch {
                                attributedText.addAttribute(.foregroundColor, value: color, range: match.range(at: index))
                                
                                if underlineColor != nil {
                                    attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: match.range(at: index))
                                    attributedText.addAttribute(.underlineColor, value: underlineColor!, range: match.range(at: index))
                                }
                            }
                            
                            variableName = String(string[s..<e])
                        } else {
                            variable?(variableName, String(string[s..<e]))
                        }
                    }
                } else {
                    if !justSearch {
                        attributedText.addAttribute(.foregroundColor, value: color, range: match.range(at: index))
                        
                        if underlineColor != nil {
                            attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: match.range(at: index))
                            attributedText.addAttribute(.underlineColor, value: underlineColor!, range: match.range(at: index))
                        }
                    }
                }
            }
        }
    }
    
}
