//
//  RenderExpression.swift
//  NoteCalc
//
//  Created by Aleksei Pugachev on 18.03.2020.
//  Copyright © 2020 Aleksei Pugachev. All rights reserved.
//

import Foundation

// MARK: Constants

let kRenderExpressionTimeWord = "Time"
let kRenderExpressionTimeWordAlt = "Now"
let kRenderExpressionInWord = "in"
let kRenderExpressionTimeRuWord = "Время"
let kRenderExpressionTimeRuWordAlt = "Сейчас"
let kRenderExpressionInRuWord = "в"
let kRenderExpressionUSDWord = "USD"

class RenderExpression {
    
    // MARK: Variables
    
    var em: Double?
    
    var userLine = false
    
    var parts: [String] = []
    var operations: [String] = []
    
    var preparedData: RendererData!
    
    init(_ line: String, rendererPreparedData: RendererData, user: Bool = false, em: Double = 16) {
        preparedData = rendererPreparedData
        userLine = user
        
        self.em = em
        
        reinit(line: line)
    }
    
    private func reinit(line: String) {
        parts = []
        operations = []
        
        var line = line
        
        line = line.replace(pattern: #"^(.*:) "#)
        line = line.replace(pattern: #"\/\/.*"#)
        
        line = line.replace(pattern: #" = (.*)"#)
        line = line.replace(pattern: #" \+= (.*)"#)
        line = line.replace(pattern: #" \-= (.*)"#)
        line = line.replace(pattern: #" \*= (.*)"#)
        line = line.replace(pattern: #" \\= (.*)"#)
        
        for constant in renderDataConstants.keys {
            line = line.replace(with: String(renderDataConstants[constant]!), pattern: "(\\b ?| )\(constant)( ?\\b| )")
        }
        
        for timezone in renderDataTimezones {
            line = line.replacingOccurrences(of: timezone, with: timezone.split(separator: "/").last!.replacingOccurrences(of: "_", with: " ").replacingOccurrences(of: "-", with: " "))
        }
            
        line = solveBrackets(line)
        
        var operations: [(NSRange,String)] = []
        
        for operation in renderDataOperators.keys {
            if let matches = line.matches(withPattern: "(\\b ?| )\(operation)( ?\\b| )") {
                for match in matches {
                    let match = match.range(at: 0)
                    let intersections = operations.map { $0.0 }.filter { $0.intersection(match) != nil }
                    
                    if intersections.count == 0 {
                        operations.append((match, renderDataOperators[operation]!))
                    } else {
                        if intersections.first!.length < match.length {
                            operations.remove(at: operations.firstIndex(where: { $0.0 == intersections.first! })!)
                            operations.append((match, renderDataOperators[operation]!))
                        }
                    }
                }
            }
        }
        
        if operations.count == 0 {
            parts.append(line)
            
            checkPartsAndOperations()
            
            if userLine { incapsulateDollarConversionIfNeeded() }
            
            return
        }
        
        operations.sort(by: { $0.0.location < $1.0.location })
        
        self.operations = operations.map { $0.1 }
        
        var offset = 0
        
        for (i, operation) in operations.enumerated() {
            let start = line.index(line.startIndex, offsetBy: offset)
            let end = line.index(line.startIndex, offsetBy: operation.0.location)
            
            if line.startIndex <= start && line.endIndex >= end && end > start {
                let partOne = "\(line[start..<end])"
                
                if (partOne.lowercased() != kRenderExpressionTimeWord.lowercased() && partOne.lowercased() != kRenderExpressionTimeRuWord.lowercased() && partOne.lowercased() != kRenderExpressionTimeWordAlt.lowercased() && partOne.lowercased() != kRenderExpressionTimeRuWordAlt.lowercased()) || (operation.1 != kRenderExpressionInWord && operation.1 != kRenderExpressionInRuWord) {
                    parts.append(partOne)
                } else {
                    self.operations.remove(at: i)
                }
                
                offset = operation.0.location + operation.0.length
                
                if i == (operations.count - 1) {
                    let start = line.index(line.startIndex, offsetBy: offset)
                    
                    parts.append("\(line[start...])")
                }
            }
        }
        
        checkPartsAndOperations()
        
        if userLine { incapsulateDollarConversionIfNeeded() }
    }
    
    // MARK: Reconstruct line
    
    private func reconstructLine() -> String {
        var line = ""
        
        for (i,part) in parts.enumerated() {
            line += part
            
            if parts.count > 1 {
                if i <= operations.count - 1 {
                   line += " \(operations[i]) "
                }
            }
        }
        
        return line
    }
    
    // MARK: Solve
    
    public func solve() -> (String?,Int)? {
        for (i,part) in parts.enumerated() {
            parts[i] = part.replacingOccurrences(of: ",", with: ".")
        }
        
        for part in parts {
            if part.matches(kRendererBackgroundIdRegex) {
                return (reconstructLine(),0)
            }
        }
        
        if parts.count == 1 && operations.count == 0 {
            if let result = isCalculatable(parts[0]) {
                return formatAnswer(result: result)
            } else {
                return nil
            }
        }
        
        var beginWithPart = 0
        
        let hasMultiply = operations.firstIndex(of: "*")
        let hasDivision = operations.firstIndex(of: "/")
        
        if hasMultiply != nil && hasDivision != nil {
            if hasMultiply! < hasDivision! {
                beginWithPart = hasMultiply!
            } else {
                beginWithPart = hasDivision!
            }
        } else if hasMultiply != nil {
            beginWithPart = hasMultiply!
        } else if hasDivision != nil {
            beginWithPart = hasDivision!
        }
        
        if parts.count == (beginWithPart + 1) {
            operations.remove(at: beginWithPart)
            
            return solve()
        }
        
        if parts.count <= (beginWithPart + 1) {
            return nil
        }
        
        if let partOne = isCalculatable(parts[beginWithPart]), let partTwo = isCalculatable(parts[beginWithPart + 1]) {
            if operations.count == 0 {
                parts[beginWithPart] = String(partOne.0) + (partOne.2.count > 0 ? " \(partOne.2)" : "")
            } else {
                if let operate = operate(partOne, partTwo, operations[beginWithPart]) {
                    parts[beginWithPart] = operate
                }
            }
            
            if !parts[beginWithPart].matches(kRendererBackgroundIdRegex) {
                if partOne.2 == partTwo.2 {
                    parts[beginWithPart] += partOne.2.count > 0 ? " \(partOne.2)" : ""
                } else {
                    if let unitExtension = arePartsOperationable(partOne, partTwo) {
                        parts[beginWithPart] += " \(unitExtension.1)"
                    } else {
                        return nil
                    }
                }
            }
        } else {
            return nil
        }
        
        parts.remove(at: beginWithPart + 1)
        
        if operations.count > 0 {
            operations.remove(at: beginWithPart)
        }
        
        return solve()
    }
    
    private func formatAnswer(result: (Double,String,String)) -> (String?,Int)? {
        let value = floor(result.0) == result.0 ? floor(result.0) : result.0
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        
        let separateNumbers = UserDefaults.standard.bool(forKey: "settingsseparateNumbers")
        
        let settingsPrecision = UserDefaults.standard.integer(forKey: "settingsanswerPrecision")
        let answerPrecision = result.1 == "Currency" ? min(settingsPrecision, 2) : settingsPrecision
        
        formatter.maximumFractionDigits = answerPrecision
        formatter.groupingSeparator = separateNumbers ? " " : ""
        
        if let formattedValue = formatter.string(from: NSNumber(value: value)) {
            switch result.1 {
                case "Percent":
                    return (formattedValue + "%",600)
                case "Double":
                    return (formattedValue,600)
                case "Unit", "Currency", "Stock":
                    return (formattedValue + " " + result.2,300)
                case "Time":
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "HH:mm"
                    dateFormatter.timeZone = result.0 == 0 ? TimeZone(identifier: result.2) : TimeZone(secondsFromGMT: 0)
                    
                    let date = result.0 == 0 ? Date() : Date(timeInterval: result.0, since: Date.init(timeIntervalSince1970: 0))
                    
                    return (dateFormatter.string(from: date),0)
                default:
                    return nil
            }
        }
        
        return nil
    }
    
    // MARK: Check parts and operations
    
    private func checkPartsAndOperations() {
        for (i, part) in parts.enumerated() {
            for operation in renderDataSoloOperators {
                let smallParts = part.components(separatedBy: operation.replacingOccurrences(of: "\\", with: "")).filter { $0.count > 0 }
                
                if smallParts.count < 2 { continue }
                
//                if i <= self.operations.count {
                    self.operations.insert(renderDataOperators[operation]!, at: i)
//                }
                
                self.parts[i] = smallParts[0].trimmingCharacters(in: .whitespacesAndNewlines)
                
                for ii in 1...smallParts.count-1 {
                    self.parts.insert(smallParts[ii].trimmingCharacters(in: .whitespacesAndNewlines), at: i + ii)
                }
                
                return checkPartsAndOperations()
            }
        }
    }
    
    // MARK: Dollar conversion for stocks and currencies
    
    private func incapsulateDollarConversionIfNeeded() {
        var isStock = ""
        
        for part in parts {
            let calculatable = isCalculatable(part)
            
            if calculatable?.1 == "Stock" {
                if isStock == "" {
                    isStock = calculatable!.2
                } else {
                    if isStock != calculatable!.2 {
                        return
                    }
                }
            }
        }
        
        if isStock != "" {
            reinit(line: "(\(reconstructLine())) \(kRenderExpressionInWord) \(kRenderExpressionUSDWord)")
        }
    }
    
    // MARK: Brackets solve
    
    private func solveBrackets(_ line: String) -> String {
        var line = line
        
        if let range = findNextBrackets(line) {
            let bracketExpression = RenderExpression(String(line[line.index(range.lowerBound, offsetBy: 1)..<range.upperBound]), rendererPreparedData: preparedData)
            if let answer = bracketExpression.solve()?.0 {
                line.replaceSubrange(range.lowerBound..<line.index(range.upperBound, offsetBy: 1), with: answer)

                return solveBrackets(line)
            }
        }
        
        return line
    }
    
    private func findNextBrackets(_ line: String) -> Range<String.Index>? {
        let openBrackets = line.indices(of: "(")
        let closeBrackets = line.indices(of: ")")
        
        if openBrackets.count == 0 || openBrackets.count != closeBrackets.count {
            return nil
        }
        
        var bracketsMap: [Int:Character] = [:]
        
        for bracket in openBrackets {
            bracketsMap[bracket] = "("
        }
        
        for bracket in closeBrackets {
            bracketsMap[bracket] = ")"
        }
        
        if let closeIndex = findCloseBracket(bracketsMap: bracketsMap) {
            return line.index(line.startIndex, offsetBy: openBrackets.first!)..<line.index(line.startIndex, offsetBy: closeIndex)
        }
        
        return nil
    }
    
    private func findCloseBracket(bracketsMap: [Int:Character], offset: Int = 1, openedBrackets: Int = 0) -> Int? {
        let sortedIndexes = bracketsMap.keys.sorted(by: { $0 < $1 })
        
        if bracketsMap[sortedIndexes.first!] != "(" {
            return nil
        }
        
        if bracketsMap[sortedIndexes[offset]] == ")" {
            if openedBrackets == 0 {
                return sortedIndexes[offset]
            } else {
                return findCloseBracket(bracketsMap: bracketsMap, offset: offset + 1, openedBrackets: openedBrackets - 1)
            }
        } else {
            return findCloseBracket(bracketsMap: bracketsMap, offset: offset + 1, openedBrackets: openedBrackets + 1)
        }
    }
    
    // MARK: Is part calculatable, return ex. (1000.0,"Currency","USD")
    
    private func isCalculatable(_ string: String) -> (Double,String,String)? {
        if string.count == 0 { return nil }
        
        let string = string.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if string.lowercased() == kRenderExpressionTimeWord.lowercased() || string.lowercased() == kRenderExpressionTimeRuWord.lowercased() || string.lowercased() == kRenderExpressionTimeWordAlt.lowercased() || string.lowercased() == kRenderExpressionTimeRuWordAlt.lowercased() {
            return (0,"Time","")
        }
        
        if string.matches(kRendererBackgroundIdRegex) {
            return (0,"Background",string)
        }
        
        if let double = Double(string.components(separatedBy: " ").joined(separator: "").components(separatedBy: " ").joined(separator: "")) {
            return (double,"Double","")
        } else {
            let words = string.components(separatedBy: " ")
            
            var possibleNumber = ""
            var parsedNumber: Double = 1
            
            for (i, word) in words.enumerated() {
                if let _ = Int(word.split(separator: " ").joined()) {} else {
                    if let double = Double(word) {
                        possibleNumber.append(String(double))
                    }
                    
                    if possibleNumber.count > 0 {
                        if let double = Double(possibleNumber) {
                            if parsedNumber == 1 { parsedNumber = double }
                            
                            if renderDataSoloUnits.keys.contains(word) {
                                if renderDataSoloUnits[word]?.0 == "multiplier" {
                                    parsedNumber = parsedNumber * Double(renderDataSoloUnits[word]!.1)
                                }
                                
                                if renderDataSoloUnits[word]?.0 == "percent" {
                                    return (parsedNumber,"Percent","")
                                }
                            }
                        }
                    }
                }
                
                let possibleString = words[i...words.count-1].joined(separator: " ").replacingOccurrences(of: " \(kRenderExpressionTimeWord.lowercased())", with: "").replacingOccurrences(of: " \(kRenderExpressionTimeRuWord.lowercased())", with: "").replacingOccurrences(of: " \(kRenderExpressionTimeWordAlt.lowercased())", with: "").replacingOccurrences(of: " \(kRenderExpressionTimeRuWordAlt.lowercased())", with: "")
                
                if preparedData.timezones.keys.contains(possibleString.lowercased()) {
                    if words.count > 1 && i > 0 {
                        let time = parseTime(words[0...i-1].joined(separator: ""))
                        return (time,"Time",preparedData.timezones[possibleString.lowercased()]!)
                    }
                    return (0,"Time",preparedData.timezones[possibleString.lowercased()]!)
                }
                
                if preparedData.timezones.keys.contains(word.lowercased()) {
                    if i == words.count - 1 {
                        if words.count > 1 {
                            let time = parseTime(words[0...i-1].joined(separator: ""))
                            return (time,"Time",preparedData.timezones[word.lowercased()]!)
                        }
                        return (0,"Time",preparedData.timezones[string]!)
                    }
                }
                
                if renderDataTimezones.contains(word) {
                    if i == words.count - 1 {
                        if words.count > 1 {
                            let time = parseTime(words[0...i-1].joined(separator: ""))
                            return (time,"Time",word)
                        }
                        return (0,"Time",word)
                    }
                }
                
                if renderDataUnitsBytes.keys.contains(possibleString) {
                    return (parsedNumber,"Unit",renderDataUnitsBytes[possibleString]!)
                }
                
                if renderDataUnitsBytes.keys.contains(word) {
                    let part = (parsedNumber,"Unit",renderDataUnitsBytes[word]!)
                    
                    if i < words.count - 1 {
                        let rest = isCalculatable(words[i+1...words.count - 1].joined(separator: " "))
                        
                        if rest != nil && rest?.1 == "Unit" {
                            if let add = operate(part, rest!, "+") {
                                if let double = Double(add) {
                                    return (double,"Unit",rest!.2)
                                }
                            }
                        }
                    }
                    
                    return part
                }
                
                if renderDataUnits.keys.contains(possibleString.lowercased()) {
                    return (parsedNumber,"Unit",renderDataUnits[possibleString.lowercased()]!)
                }
                
                if renderDataUnits.keys.contains(word.lowercased()) {
                    let part = (parsedNumber,"Unit",renderDataUnits[word.lowercased()]!)
                    
                    if i < words.count - 1 {
                        let rest = isCalculatable(words[i+1...words.count - 1].joined(separator: " "))
                        
                        if rest != nil && rest?.1 == "Unit" {
                            if let add = operate(part, rest!, "+") {
                                if let double = Double(add) {
                                    return (double,"Unit",rest!.2)
                                }
                            }
                        }
                    }
                    
                    return part
                }
                
                if renderDataStocks.contains(word.uppercased()) {
                    if i == words.count - 1 {
                        return (parsedNumber,"Stock",word.uppercased())
                    }
                }
                
                if preparedData.currenciesValues.contains(word.lowercased()) {
                    return (parsedNumber,"Currency",word.uppercased())
                }
                
                if let key = preparedData.currenciesKey.keys.firstIndex(of: possibleString.lowercased()) {
                    return (parsedNumber,"Currency",preparedData.currenciesKey[preparedData.currenciesKey.keys[key]]!)
                }
                
                if let key = preparedData.currenciesKey.keys.firstIndex(of: word.lowercased()) {
                    return (parsedNumber,"Currency",preparedData.currenciesKey[preparedData.currenciesKey.keys[key]]!)
                }
                
                for currency in renderDataCurrenciesSymbols.keys {
                    if word == currency {
                        return (parsedNumber,"Currency",renderDataCurrenciesSymbols[currency]!)
                    }
                }
                
                if let _ = Int(word) {
                    possibleNumber.append(contentsOf: word)
                } else {
                    var suffixed = false
                    
                    for unit in renderDataSoloUnits.keys {
                        if word.lowercased().hasSuffix(unit) && word.count > 1 {
                            let index = word.index(word.endIndex, offsetBy: -unit.count)
                            if let int = Int(word[word.startIndex...index]) {
                                if renderDataSoloUnits[unit]?.0 == "multiplier" {
                                    possibleNumber.append(String(int * renderDataSoloUnits[unit]!.1))
                                } else {
                                    possibleNumber.append(String(int))
                                }
                                
                                if renderDataSoloUnits[unit]?.0 == "percent" {
                                    if let double = Double(possibleNumber) {
                                        return (double,"Percent","")
                                    }
                                }
                                
                                suffixed = true
                            } else {
                                if renderDataSoloUnits[unit]?.0 == "percent" {
                                    if let double = Double(word[word.startIndex...index]) {
                                        return (double,"Percent","")
                                    }
                                }
                            }
                        }
                    }
                    
                    for function in renderDataSoloFunctions.keys {
                        if word.lowercased().hasPrefix(function) || word.lowercased() == function {
                            let string = (word.replacingOccurrences(of: function, with: "") + (words.count - 1 > i ? " " +  words[i+1...words.count-1].joined(separator: " ") : "")).trimmingCharacters(in: .whitespacesAndNewlines)
                            if let double = Double(string) {
                                if let function = renderDataFunctionsClosures[renderDataSoloFunctions[function]!] {
                                    return (function(double),"Double","")
                                }
                            }
                        }
                    }
                    
                    for function in renderDataFunctions.keys {
                        if word.lowercased() == function && words.count - 1 > i {
                            let string = words[i+1...words.count-1].joined(separator: " ").trimmingCharacters(in: .whitespacesAndNewlines)
                            if let double = Double(string) {
                                if let function = renderDataFunctionsClosures[renderDataFunctions[function]!] {
                                    return (function(double),"Double","")
                                }
                            } else {
                                let parts = string.split(separator: " ")
                                
                                if parts.count == 2 {
                                    if let double1 = Double(parts[0]), let double2 = Double(parts[1]) {
                                        if let function = renderDataFunctionsDoubleValueClosures[renderDataFunctions[function]!] {
                                            return (function(double1,double2),"Double","")
                                        }
                                    }
                                }
                                
                                let commaparts = string.split(separator: "|")
                                
                                if commaparts.count > 0 {
                                    var doubles: [Double] = []
                                    
                                    var uniformityType: String?
                                    var uniformityClass: String?
                                    
                                    for part in commaparts {
                                        if let data = isCalculatable(String(part)) {
                                            if (uniformityType == nil && uniformityClass == nil) || (uniformityType == data.1 && uniformityClass == data.2) {
                                                uniformityType = data.1
                                                uniformityClass = data.2
                                                
                                                doubles.append(data.0)
                                            } else {
                                                return nil
                                            }
                                        }
                                    }
                                    
                                    if doubles.count > 0, let type = uniformityType, let classs = uniformityClass {
                                        if let function = renderDataFunctionsArrayValueClosures[renderDataFunctions[function]!] {
                                            return (function(doubles),type,classs)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    for symbol in renderDataCurrenciesSymbols.keys {
                        if word.hasSuffix(symbol) || word.hasPrefix(symbol) {
                            if let double = Double(possibleNumber) {
                                return (double,"Currency",renderDataCurrenciesSymbols[symbol]!)
                            }
                            
                            let word = word.replacingOccurrences(of: symbol, with: "").lowercased()
                            
                            if let double = Double(word) {
                                return (double,"Currency",renderDataCurrenciesSymbols[symbol]!)
                            } else {
                                for unit in renderDataSoloUnits.keys {
                                    if word.hasSuffix(unit) {
                                        if word.replacingOccurrences(of: unit, with: "").count > 0 {
                                            let index = word.index(word.endIndex, offsetBy: -unit.count-1)
                                            
                                            if let int = Int(word[word.startIndex...index]) {
                                                if renderDataSoloUnits[unit]?.0 == "multiplier" {
                                                    possibleNumber.append(String(int * renderDataSoloUnits[unit]!.1))
                                                } else {
                                                    possibleNumber.append(String(int))
                                                }
                                                
                                                if let double = Double(possibleNumber) {
                                                    return (double,"Currency",renderDataCurrenciesSymbols[symbol]!)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    if suffixed {
                        return isCalculatable(possibleNumber + " " + ((i < words.count - 1) ? words[(i+1)...(words.count-1)].joined(separator: " ") : ""))
                    }
                }
            }
        }
        
//        print("Filtered part: '\(string)'")
        
        return nil
    }
    
    // MARK: Are parts operationable, returns final type ex. ("Currency","USD")
    
    private func arePartsOperationable(_ partOne: (Double,String,String), _ partTwo: (Double,String,String)) -> (String,String)? {
        if partOne.1 == "Double" {
            return (partTwo.1,partTwo.2)
        } else if partTwo.1 == "Double" {
            return (partOne.1,partOne.2)
        } else {
            if partOne.1 == partTwo.1 {
                return (partTwo.1,partTwo.2)
            }
        }
        
        if partOne.1 == "Percent" && partTwo.1 == "Percent" {
            return ("Percent","")
        } else {
            if partOne.1 == "Percent" {
                return (partTwo.1,partTwo.2)
            }
            
            if partTwo.1 == "Percent" {
                return (partOne.1,partOne.2)
            }
        }
        
        if (partOne.1 == "Stock" && partTwo.1 == "Currency") || (partOne.1 == "Currency" && partTwo.1 == "Stock") {
            return (partTwo.1,partTwo.2)
        }
        
        return nil
    }
    
    // MARK: Parse time
    
    private func parseTime(_ string: String) -> Double {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mma"
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        if let possibleTime12 = dateFormatter.date(from: string)?.timeIntervalSince1970 {
            return possibleTime12
        }
        
        dateFormatter.dateFormat = "HH:mm"
        
        if let possibleTime24 = dateFormatter.date(from: string)?.timeIntervalSince1970 {
            return possibleTime24
        }
        
        return 0
    }
    
    // MARK: Operate two parts
    
    private func operate(_ partOne: (Double,String,String), _ partTwo: (Double,String,String), _ operation: String) -> String? {
        if partOne.1 == "Percent" || partTwo.1 == "Percent" {
            return operatePercents(partOne, partTwo, operation)
        }
        
        if let type = arePartsOperationable(partOne, partTwo) {
            switch operation {
                case "in":
                    switch partTwo.1 {
                        case "Time":
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            dateFormatter.timeZone = TimeZone.init(secondsFromGMT: 0)
                            
                            let diff = (partOne.2 == "" ? 0 : TimeZone(identifier: partOne.2)!.secondsFromGMT()) - TimeZone(identifier: partTwo.2)!.secondsFromGMT()
                            
                            let date = partOne.0 == 0 ? Date() : Date(timeInterval: partOne.0, since: Date.init(timeIntervalSince1970: TimeInterval(-diff)))
                            
                            return dateFormatter.string(from: date)
                        case "Unit":
                            for chain in renderDataConvertChains {
                                if let amountOne = partOne.2 == "em" ? em : chain[partOne.2], let amountTwo = partTwo.2 == "em" ? em : chain[partTwo.2] {
                                    return String(amountOne * partOne.0 / amountTwo)
                                }
                            }
                            
                            if renderDataConvertFormulas.keys.contains("\(partOne.2)\(partTwo.2)") {
                                if let calculatedFormula = RenderExpression(renderDataConvertFormulas["\(partOne.2)\(partTwo.2)"]!.replacingOccurrences(of: "x", with: String(partOne.0)), rendererPreparedData: preparedData).solve()?.0 {
                                    return calculatedFormula
                                }
                            }
                        break
                        case "Currency":
                            return backgroundCalculation(partOne, partTwo)
                        case "Stock":
                            return backgroundCalculation(partOne, partTwo)
                        default: break
                    }
                break
                default:
                    var partOneValue: Double? = nil
                    
                    if partOne.1 != "Double" && partTwo.1 != "Double" {
                        switch type.0 {
                            case "Unit":
                                for chain in renderDataConvertChains {
                                    if let amountOne = partOne.2 == "em" ? em : chain[partOne.2], let amountTwo = partTwo.2 == "em" ? em : chain[partTwo.2] {
                                        partOneValue = amountOne * partOne.0 / amountTwo
                                    }
                                }
                                
                                if renderDataConvertFormulas.keys.contains("\(partOne.2)\(partTwo.2)") {
                                    if let calculatedFormula = RenderExpression(renderDataConvertFormulas["\(partOne.2)\(partTwo.2)"]!.replacingOccurrences(of: "x", with: String(partOne.0)), rendererPreparedData: preparedData).solve()?.0, let calculatedFormulaDouble = Double(calculatedFormula) {
                                        partOneValue = calculatedFormulaDouble
                                    }
                                }
                            break
                            case "Currency":
                                return backgroundCalculation(partOne, partTwo, operation)
                            case "Stock":
                                return backgroundCalculation(partOne, partTwo, operation)
                            default: break
                        }
                    } else {
                        partOneValue = partOne.0
                    }
                    
                    if partOneValue == nil { return nil }
                    
                    switch operation {
                        case "+":
                            return String(partOneValue! + partTwo.0)
                        case "-":
                            return String(partOneValue! - partTwo.0)
                        case "*":
                            return String(partOneValue! * partTwo.0)
                        case "/":
                            if partTwo.0 == 0 { return nil }
                            
                            return String(partOneValue! / partTwo.0)
                        case "^":
                            return String(pow(partOneValue!, partTwo.0))
                        case "as":
                            return String(partOneValue! / partTwo.0 * 100) + "%"
                        default: break
                    }
                break
            }
        }
        
        return nil
    }
    
    // MARK: Operate percents
    
    private func operatePercents(_ partOne: (Double,String,String), _ partTwo: (Double,String,String), _ operation: String) -> String? {
        if partOne.1 == "Percent" && partTwo.1 == "Percent" {
            switch operation {
                case "+":
                    return String(partOne.0 + partTwo.0)
                case "-":
                    return String(partOne.0 - partTwo.0)
                case "*":
                    return String(partOne.0 * partTwo.0)
                case "/":
                    if partTwo.0 == 0 { return nil }
                    
                    return String(partOne.0 / partTwo.0)
                case "^":
                    return String(pow(partOne.0, partTwo.0))
                default: break
            }
        } else {
            let partOneIsPercent = partOne.1 == "Percent"
            
            switch operation {
                case "+":
                    if partOneIsPercent {
                        return String(partTwo.0 * partOne.0 / 100 + partTwo.0)
                    } else {
                        return String(partOne.0 + partTwo.0 * partOne.0 / 100)
                    }
                case "-":
                    if partOneIsPercent {
                        return nil
                    } else {
                        return String(partOne.0 - partTwo.0 * partOne.0 / 100)
                    }
                case "*":
                    if partOneIsPercent {
                        return String(partTwo.0 * partOne.0 / 100 * partTwo.0)
                    } else {
                        return String(partOne.0 * partTwo.0 * partOne.0 / 100)
                    }
                case "/":
                    if partTwo.0 == 0 { return nil }
                    
                    if partOneIsPercent {
                        return String(partTwo.0 * partOne.0 / 100 / partTwo.0)
                    } else {
                        return String(partOne.0 / (partTwo.0 * partOne.0 / 100))
                    }
                case "^":
                    if partOneIsPercent {
                        return String(pow(partTwo.0 * partOne.0 / 100, partTwo.0))
                    } else {
                        return String(pow(partOne.0, partTwo.0 * partOne.0 / 100))
                    }
                case "of":
                    return String(partTwo.0 * partOne.0 / 100)
                default: break
            }
        }
        
        return nil
    }
    
    // MARK: Background calculations
    
    private func backgroundCalculation(_ partOne: (Double,String,String), _ partTwo: (Double,String,String), _ operation: String? = nil) -> String {
        let id = String((0..<24).map { _ in kAppGeneratorLetters.randomElement()! })
        
        switch partTwo.1 {
            case "Currency":
                switch partOne.1 {
                    case "Currency":
                        if partOne.2 == partTwo.2 {
                            switch operation {
                                case "+":
                                    return String(partOne.0 + partTwo.0) + " " + partTwo.2
                                case "-":
                                    return String(partOne.0 - partTwo.0) + " " + partTwo.2
                                case "*":
                                    return String(partOne.0 * partTwo.0) + " " + partTwo.2
                                case "/":
                                    if partTwo.0 == 0 { return "" }
                                    
                                    return String(partOne.0 / partTwo.0) + " " + partTwo.2
                                case "^":
                                    return String(pow(partOne.0, partTwo.0)) + " " + partTwo.2
                                default: break
                            }
                        }
                        
                        let pair = "\(partOne.2 == kRenderExpressionUSDWord ? "" : partOne.2)\(partTwo.2)=X"

                        Network.shared.getStockUSDPrices([pair], completion: { prices in
                            if let price = prices[pair] {
                                var result = partOne.0 * price
                                
                                switch operation {
                                    case "+":
                                        result += partTwo.0
                                    break
                                    case "-":
                                        result -= partTwo.0
                                    break
                                    case "*":
                                        result *= partTwo.0
                                    break
                                    case "/":
                                        if partTwo.0 == 0 { return }
                                        
                                        result /= partTwo.0
                                    break
                                    case "^":
                                        result = pow(result, partTwo.0)
                                    break
                                    default: break
                                }
                                
                                (Renderer.secondary != nil ? Renderer.secondary! : Renderer.shared).fillBackgroundId(id, value: String(result) + " " + partTwo.2)
                            }
                        })
                    break
                    case "Stock":
                        var pairs = [partOne.2.uppercased()]
                        
                        if partTwo.2 != kRenderExpressionUSDWord {
                            pairs.append("\(partTwo.2)=X")
                        }
                        
                        Network.shared.getStockUSDPrices(pairs, completion: { prices in
                            var rate: Double = 1
                            
                            if pairs.count > 1 {
                                if let price = prices[pairs[1]] {
                                    rate = price
                                }
                            }
                            
                            if let price = prices[pairs[0]] {
                                var result = partOne.0 * price * rate
                                
                                switch operation {
                                    case "+":
                                        result += partTwo.0 * rate
                                    break
                                    case "-":
                                        result -= partTwo.0 * rate
                                    break
                                    case "*":
                                        result *= partTwo.0 * rate
                                    break
                                    case "/":
                                        if partTwo.0 == 0 { return }
                                        
                                        result /= partTwo.0 * rate
                                    break
                                    case "^":
                                        result = pow(result, partTwo.0 * rate)
                                    break
                                    default: break
                                }
                                
                                (Renderer.secondary != nil ? Renderer.secondary! : Renderer.shared).fillBackgroundId(id, value: String(result) + " " + partTwo.2)
                            }
                        })
                    break
                    default: break
                }
            break
            case "Stock":
                switch partOne.1 {
                    case "Currency":
                        var pairs = [partTwo.2.uppercased()]
                        
                        if partOne.2 != kRenderExpressionUSDWord {
                            pairs.append("\(partOne.2)=X")
                        }
                        
                        Network.shared.getStockUSDPrices(pairs, completion: { prices in
                            var rate: Double = 1
                            
                            if pairs.count > 1 {
                                if let price = prices[pairs[1]] {
                                    rate = price == 0 ? 1 : price
                                }
                            }
                            
                            if let price = prices[pairs[0]] {
                                var result = partOne.0 / (price == 0 ? 1 : price) / rate
                                
                                switch operation {
                                    case "+":
                                        result += partTwo.0
                                    break
                                    case "-":
                                        result -= partTwo.0
                                    break
                                    case "*":
                                        result *= partTwo.0
                                    break
                                    case "/":
                                        if partTwo.0 == 0 { return }
                                        
                                        result /= partTwo.0
                                    break
                                    case "^":
                                        result = pow(result, partTwo.0)
                                    break
                                    default: break
                                }
                                
                                (Renderer.secondary != nil ? Renderer.secondary! : Renderer.shared).fillBackgroundId(id, value: String(result) + " " + partTwo.2)
                            }
                        })
                    break
                    case "Stock":
                        Network.shared.getStockUSDPrices([partOne.2, partTwo.2], completion: { prices in
                            if let partOnePrice = prices[partOne.2], let partTwoPrice = prices[partTwo.2] {
                                var result = partOne.0 * partOnePrice / partTwoPrice
                                
                                switch operation {
                                    case "+":
                                        result += partTwo.0
                                    break
                                    case "-":
                                        result -= partTwo.0
                                    break
                                    case "*":
                                        result *= partTwo.0
                                    break
                                    case "/":
                                        if partTwo.0 == 0 { return }
                                        
                                        result /= partTwo.0
                                    break
                                    case "^":
                                        result = pow(result, partTwo.0)
                                    break
                                    default: break
                                }
                                
                                (Renderer.secondary != nil ? Renderer.secondary! : Renderer.shared).fillBackgroundId(id, value: String(result) + " " + partTwo.2)
                            }
                        })
                    break
                    default: break
                }
            break
            default: break
        }
        
        return "~###\(id)###~"
    }
    
}
